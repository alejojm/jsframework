/**
* behavior for inputs with labels inside
*/
function formInputLabel(options){
	this.options = options;
	this.labels = {};
	this.set();
	this.init();
}

formInputLabel.render = function(){
	var forms = jQuery('.formInputLabel');
	for (var a=0; a < forms.length; a++) {
		if(!forms[a].formInputLabel)
			forms[a].formInputLabel = new formInputLabel({form:forms[a]});
	}
	forms = null;
};

formInputLabel.prototype.set = function() {
	this.selector = 'input:text, textarea';
	this.form = null;
};

formInputLabel.prototype.init = function() {
	this.form = jQuery(this.options.form);
	this.form.find(this.selector).focusin(jQuery.proxy(this.focusin,this));
	this.form.find(this.selector).focusout(jQuery.proxy(this.focusout,this));
	this.form.submit(jQuery.proxy(this.submit, this));
	this.form.on('validationComplete', jQuery.proxy(this.validationComplete, this));
	this.addLabels();
};

formInputLabel.prototype.addLabels = function(params) {
	var inputs = this.form.find(this.selector);
	for (var a=0; a < inputs.length; a++) if(!this.labels[a]) {
		this.labels[a] = inputs[a].value;
		inputs[a].position = a;
	}
};

formInputLabel.prototype.refresh = function() {
	this.form.find(this.selector).focusin(jQuery.proxy(this.focusin,this));
	this.form.find(this.selector).focusout(jQuery.proxy(this.focusout,this));
	this.addLabels();
};

formInputLabel.prototype.submit = function(e) {
	var inputs = this.form.find(this.selector);
	for (var a=0; a < inputs.length; a++) {
		if(this.labels[a] == inputs[a].value && !jQuery(inputs[a]).hasClass("formInputLabelDisable"))
			inputs[a].value = '';
	}
};

formInputLabel.prototype.validationComplete = function(e, wasValid) {
	var inputs = this.form.find(this.selector);
	for (var a=0; a < inputs.length; a++) {
		for(var label in this.labels){
			if(!jQuery(inputs[a]).hasClass("formInputLabelDisable") && inputs[a].value === '')
				inputs[a].value = this.labels[a];
		}
	}
};

formInputLabel.prototype.focusin = function(e) {
	var position = e.target.position;
	var target = jQuery(e.target);
	if(!target.hasClass("formInputLabelDisable") && target.val() == this.labels[position])
		target.val('');
};

formInputLabel.prototype.focusout = function(e) {
	var position = e.target.position;
	var target = jQuery(e.target);
	if(!target.hasClass("formInputLabelDisable") && target.val() === "")
		target.val(this.labels[position]);
};

/**
* Find all elements that have class inputLabel and fire
*/
jQuery(document).ready(function(){formInputLabel.render();});