/*
 * WebView Object
 */
function webView(options) {
	this.options = options;
	this.init();
	this.set();
}

webView.prototype.init = function() {
	$('.backbtn').bind('mousedown', $.proxy(this.clickBackBtn, this));
	$(window).bind('hashchange', $.proxy(this.hashChange, this));
};

webView.prototype.set = function() {
	this.urlMobile  = Config.qs.replace('[VERSION]', Config.version);
	this.frame      = null;
	this.content    = null;
	this.scroll     = null;
	this.onComplete = null;
	this.className  = null;
	this.cache      = [];
};

webView.prototype.loadUrl = function(source, onComplete) {

	if (onComplete)
		this.options.onComplete = onComplete;

	this.options.src = source;

	//navigation has the controll of this btn
	if (this.cache.length > 1)
		$('.backbtn').css({opacity: 0}).removeClass('hide').transition({opacity: 1});
		// TODO make this work
		//new Timer({complete:$.proxy(this.activateBackBtn,this)});

	if (!$('.' + this.options.className).length) {
		Application.loading();

		var className = this.options.className ? 'class="vistas hide ' + this.options.className + '"' : 'class="vistas"';
		var cachedResponse = localCache.get(this.options.src);
		if(cachedResponse){
			this.frame = "<iframe frameborder='0' allowtransparency='true' " + className + "'></iframe>";
			$('#wrapper').append(this.frame);
			this.renderCache(cachedResponse);
		}
		else
		{
			this.frame = "<iframe frameborder='0' allowtransparency='true' " + className + " src='" + source + "'></iframe>";
			$('#wrapper').append(this.frame);
		}
		$('.' + this.options.className).load($.proxy(this.loadingComplete, this));

	}	else {

		if (this.options.onComplete)
			this.options.onComplete();
	}
};

webView.prototype.loadingComplete = function(e) {
	$('.' + this.options.className).css({width:$(window).width(), height:$(window).height()});
	this.content = $('.' + this.options.className).contents();

	if(this.scroll)
		$(this.content).scrollTop(this.scroll);

	if (this.options.loadManager)
		this.addEventListeners();

	$('.' + this.options.className).removeClass('hide');
	this.addCache($('.' + this.options.className).attr('src'));

	if (this.cache.length > 1)
		$('.backbtn').css({opacity: 0}).removeClass('hide').transition({opacity: 1});

	Application.loadingComplete();

	// Execute one time the callback
	if (this.options.onComplete) {
		var callback = this.options.onComplete;
		this.options.onComplete = null;
		callback();
	} else if (typeof this.options.delegate.reload == 'function') {
		this.options.delegate.reload();
	}
};

webView.prototype.addCache = function(url) {
	//save in localstorga
	var html = this.content.find('html').html();
	localCache.add(this.options.src, html, true);

	var exists = false;
	for (var a=0; a<this.cache.length; a++) {
		if (this.cache[a] == url)
			exists = true;
	}

	if (!exists)
		this.cache.push(url);
};

webView.prototype.clearCache = function() {
	localCache.remove(this.options.src);
	this.cache = [];
	this.content = null;
	$('.'+this.options.className).remove();
};

webView.prototype.renderCache = function(cachedResponse) {
	var iframe = $('.' + this.options.className).get(0);
	var doc = iframe.document;

	if (iframe.contentDocument)
		doc = iframe.contentDocument; // For NS6
	else if(iframe.contentWindow)
		doc = iframe.contentWindow.document; // For IE5.5 and IE6

	// Put the content in the iframe
	doc.open();
	doc.writeln(cachedResponse);
	doc.close();
};

webView.prototype.addEventListeners = function() {
	$(this.content).find('a').not(".linkApp, .videoWebView").bind('mousedown', $.proxy(this.clickOnLink, this));
	$(this.content).find('select.selectWebView').bind('change', $.proxy(this.changeSelectWebView, this));
};

webView.prototype.clickOnLink = function(e) {
	e.preventDefault();

	var target = $(e.target).is('a') ? $(e.target) : $(e.target).parent();
	if(target.hasClass('androidAppBrowser') && Application.isAndroid() && Application.androidAppVersion >= "1.7"){
		AndroidApp.openExternalURL(target.attr('href'));
		return;
	}
	Application.loading();
	var href = target.attr('href');
	var a = document.createElement('a');
	a.href = href;

	var hostname  = a.host;
	var proto     = a.protocol;
	var pathname  = a.pathname;
	var path      = this.options.path ? '/' + this.options.path : '';
	var extrapath = path + pathname;
	extrapath     = extrapath.substr(0, (extrapath.length-1));

	if (a.search === "")
		a.search = this.urlMobile;
	else
		a.search += this.urlMobile.replace(/\?/, '&');

	extrapath = extrapath+a.search;
	if(hostname != location.host){
		extrapath = '/proxy/'+hostname+extrapath;
	}

	$('.' + this.options.className).attr('src', extrapath);

	if(this.cache.length > 0){
		Url.setUrlVar("page", this.cache.length);
	}

	this.scroll = $(this.content).scrollTop();
	$('.backbtn').addClass('hide');
};

webView.prototype.changeSelectWebView = function(e) {
	e.preventDefault();
	Application.loading();
	var selectedValue = $(e.target).find(':selected').val();

	if (selectedValue != '#') {
		var href = selectedValue;
		var pathname = href.replace(/^.*\/\/[^\/]+/, '');
		var domain = pathname;

		if (domain.search(/\?/) != -1)
			domain += this.urlMobile.replace(/\?/, '&');
		else
			domain += this.urlMobile;

		$('.' + this.options.className).attr('src', domain);
		$('.backbtn').addClass('hide');
	}
};

webView.prototype.clickBackBtn = function(e) {
	if (this.options.delegate.name == Url.getHashController() &&
		this.cache.length > 0 &&
		$('.' + this.options.className).is(":visible")
	) {
		var before = this.cache.length - 2;
		var url = this.cache[before];
		this.cache.splice((this.cache.length - 1), 1);
		$('.' + this.options.className).attr('src', url);
		$('.backbtn').addClass('hide');
		Application.loading();
	}
};

webView.prototype.hashChange = function(e) {
	var page = Url.getHashVar('page');
	//if page is diferent from cache is bacause the back btn was clicked
	if(	page && page != this.cache.length &&
		$('.' + this.options.className).is(":visible")
	)
	{
		this.clickBackBtn();
	}
};


webView.prototype.activateBackBtn = function(e) {
	$('.backbtn').css({opacity: 0}).removeClass('hide').transition({opacity: 1});
};
