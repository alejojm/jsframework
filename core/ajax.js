/**
 * Wrapper ajax, we need every request be a unique object
 * if upload support is required include the plugin http://www.malsup.com/jquery/form/#download
 */
function Ajax(options, formOrSuccess, success, params) {
	this.set(options, formOrSuccess, success, params);
	this.init();
}

Ajax.suscribe = function(url, callback){
	if(!Ajax.subscribers)
		Ajax.subscribers = {};
	Ajax.subscribers[url] = callback;
};

Ajax.prototype.set = function(options, formOrSuccess, success, params) {
	this.errorMsg   = window['Config'] && Config.ajaxError ? Config.ajaxError : 'Esperando conexión ...';
	this.timeout    = null;
	this.progress   = 0;
	this.count      = 0;
	this.wait       = 500;
	this.countError = 1;
	this.base       = window['Config'] && Config.webroot ? Config.webroot : '';

	//show and hide loading
	this.loading = null;
	this.loadingOptions = {
		selector:'.load, .loading',
		classNames:'invisible hide'
	};

	if(typeof options == 'string'){
		this.options = {};
		this.options.url = options;
		this.options.success = success ? success : formOrSuccess;
		this.options.form = success ? formOrSuccess : null;
		this.options.params = params ? params : null;
	}
	else
		this.options = options;

	//get the context from setup, if not set in the current ajax
	if(!this.options.context){
		var setup = jQuery.ajaxSetup();
		if(setup.context)
			this.options.context = setup.context;
	}
};

Ajax.prototype.init = function() {
	//is from timeout
	if(this.timeout) clearTimeout(this.timeout);

	if (this.options.form || this.options.json)
		this.post();
	else
		this.get();
};

Ajax.prototype.get = function() {
	jQuery.ajax({
		url: this.base+this.options.url,
		type: 'GET',
		dataType: 'json',
		success: this.response,
		error: this.error,
		context: this
	});
};

Ajax.prototype.post = function() {

	var data = this.options.form;

	if(this.options.form && this.options.form.is && this.options.form.is('form')){
		data = this.options.form.serialize();
		this.loading = this.options.form.find(this.loadingOptions.selector);
		if(this.loading.length)
			this.loading.removeClass(this.loadingOptions.classNames);
	}

	else if(!this.options.json && typeof data != 'string')
		data = jQuery.param(this.options.form);

	var processData = true;
	var contentType = "application/x-www-form-urlencoded; charset=UTF-8";

	data = this.setSecureFields(data);

	if(this.options.json){
		processData = false;
		data = JSON.stringify(this.options.json);
		contentType = "application/json; charset=utf-8";
	}

	var options = {
		url: this.base+this.options.url,
		type: 'POST',
		data: data,
		dataType: 'json',
		contentType:contentType,
		processData:processData,
		success: this.response,
		error: this.error,
		context: this
	};
	var isFile = this.options.form && this.options.form.find && this.options.form.find('input:file').length;
	if(isFile && this.options.form.ajaxSubmit){
		options.data = jQuery(this.options.form).fieldSerialize();
		this.options.form.ajaxSubmit(options);
	}	else {
		jQuery.ajax(options);
	}

};

/**
 * test if the context have getSecure method
 * @return {mixed} string or object
 * @author Alejo JM <alejo.jm@gmail.com> October 24, 2013
 */
Ajax.prototype.setSecureFields = function(data) {
	return data;
	//TODO make this work
	// if(!this.options.context)
	// 	return data;


	// if(typeof this.options.context.ajaxSecureFields != 'undefined'){
	// 	if(typeof this.options.context.ajaxSecureFields == 'function')
	// 		data  = this.addSecureField(data, this.options.context.ajaxSecureFields(this.options.url));
	// 	else
	// 		data  = this.addSecureField(data, this.options.context.ajaxSecureFields);
	// }

	// return data;
};

Ajax.prototype.addSecureField = function(data, fields) {
	// debugger;
};

Ajax.prototype.response = function(response) {
	//was error or not remove this div, set initial values
	jQuery('#ajax-internal-error').remove();
	this.countError = 1;
	this.progress = 0;

	//hide loading
	if(this.loading && this.loading.length)
		this.loading.addClass(this.loadingOptions.classNames);

	var params = this.options.params ? this.options.params : {};

	//the server send a redirect, process here
	if(response.ajax_internal_redirect){
		//the context have redirect function
		if(this.options.context && typeof this.options.context.ajaxRedirect == 'function'){
			return this.options.context.ajaxRedirect.call(this.options.context, response, this.options);
		}
		window.location = response.ajax_internal_redirect;
		return;
	}


	this.options.success.call(this.options.context, response, params);

	var renderUrl = this.options.url.replace(/\//g, '');

	//tell everyone interested in this ajax reponse
	jQuery(window).trigger(renderUrl+'_reponse', response);

	//maybe someone suscribe to this response
	if(Ajax.subscribers && Ajax.subscribers[renderUrl]){
		Ajax.subscribers[renderUrl](response, params);
	}

	//save in localStorage if is not a POST and is activated
	response = null;
};

Ajax.prototype.error = function(xhr, textStatus, error) {
	if(window['Config'] && Config.debug){
		return console.log('AJAX error:'+error+' url:'+this.options.url+' textStatus:'+textStatus);
	}

	//add delay bewteens calls
	if(jQuery('#ajax-internal-error').length){
		this.countError++;
	}

	if(this.countError > 10)
		this.countError = 1;

	this.createError();
	this.loadingIndicator();
	this.timeout = setTimeout(jQuery.proxy(this.init,this), this.wait*this.countError);
};


Ajax.prototype.createError = function() {
	if(jQuery('#ajax-internal-error').length) return;
	var divError = '<div id="ajax-internal-error" style="position:absolute;top:2px;z-index:999;width:100%;height:25px;background:#ff0000;color:#FFF;font-family: Arial;font-size:14px;padding:7px 0 0 10px;-moz-box-shadow: #CCC 0px 0px 5px;-webkit-box-shadow: #CCC 0px 0px 5px;box-shadow: #CCC 0px 0px 5px;"> <div id="ajax-internal-message" style="float:left;">{ajax_msg_error}</div><div id="ajax-internal-progress" style="float:left;padding:6px 0 0 10px;width:0"><div style="width:100%;height:5px;background:#FFF;border:1px solid #FFF;-moz-border-radius:5px;-webkit-border-radius: 5px;border-radius: 5px;"></div></div></div>';
	jQuery('body').append(divError.replace('{ajax_msg_error}', this.errorMsg));
};

Ajax.prototype.loadingIndicator = function () {
	if(!jQuery('#ajax-internal-error').length){
		this.progress = 0;
		return;
	}

	this.progress = this.progress+10;
	var remain = jQuery(window).width() - jQuery('#ajax-internal-message').width() - 20;
	if(this.progress >= remain){
		this.progress = 0;
		jQuery('#ajax-internal-progress').css({width:'0'});
		return this.loadingIndicator();
	}

	jQuery('#ajax-internal-progress').animate({width:this.progress}, {duration:500, complete:jQuery.proxy(this.loadingIndicator,this)});
};
