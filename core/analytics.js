/**
 * Track url using google analitycs
 */
var Analytics = {

	track: function(url) {
		// Indicar en la URL si el usuario esta logueado
		if (Application.controllers.Users.isLogged)
			url += 'usuario-activo';

		// Es el wrapper the android ?
		if (Application.isAndroid())
			url = url.replace(/mobile/i, "android");


		// Debemos hacer log de la URL?
		if (Config.log_analytics) {
			if (window._gaq)
				Log.console("Analytics.track:" + url);
			else
				throw new Error('No esta incluido, google analitycs ojo !!!!!');
		}

		// Debemos hacer track de la URL?
		if (Config.track_analytics) {
			if (window._gaq)
				_gaq._createAsyncTracker(Config.analytics_account)._trackPageview(url);
			else
				throw new Error('No esta incluido, google analitycs ojo !!!!!');
		}
	}
};
