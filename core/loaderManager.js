/**
 * Singleton to load script in one div and jquery, you must have
 * one file called javascripts.html inside on the root of your server
 * used for load all javascript
 * @type   {Object}
 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
 */
var loaderManager = {
	/**
	 * common settings
	 * @type   {Object}
	 * @author Alejo JM <alejo.jm@gmail.com> October 17, 2013
	 */
	settings:{
		/**
		 * what jquery version we need to load
		 * @type   {String}
		 * @author Alejo JM <alejo.jm@gmail.com> October 17, 2013
		 */
		version:'1.10.2',

		/**
		 * what file use for add async
		 * @type   {String}
		 * @author Alejo JM <alejo.jm@gmail.com> October 17, 2013
		 */
		file:'javascripts.html',

		/**
		 * id of the div were javascripts are included
		 * @type   {String}
		 * @author Alejo JM <alejo.jm@gmail.com> October 17, 2013
		 */
		id:'javascripts',

		/**
		 * how many miliseconds we need wait to the next timer
		 * @type   {int}
		 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
		 */
		wait:100,

		/**
		 * remove cache or activate from jquery file.js?_=timestamp
		 * @type   {Boolean}
		 * @author Alejo JM <alejo.jm@gmail.com> November 06, 2013
		 */
		cache:true
	},

	/**
	 * jquery to include
	 * @type   {String}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	cdnUrl:'http://ajax.googleapis.com/ajax/libs/jquery/[VERSION]/jquery.min.js',

	/**
	 * timeout id
	 * @type   {Number}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	timeout:0,

	/**
	 * tell us if we have external js
	 * @type   {Number}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	numberExternalJs:0,

	/**
	 * init method fire timer wait jquery is ready for use
	 * @return {[type]} [description]
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	init:function(){
		this.getUrlOptions();

		if(!this.exists()){
			this.addJquery();
			this.timer();
		}
	},

	/**
	 * get url init param override default settings, you can pass thing like
	 * loaderManager.js?init=facebookSDK,version:jqueryVersion,file=anotherfile.html
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 19, 2013
	 */
	getUrlOptions:function () {
		var me = '';
		for (var a = 0; a < document.scripts.length; a++) {
			if(document.scripts[a].src.indexOf('loaderManager.js')!==-1){
				me = document.scripts[a].src;
				break;
			}
		}
		//don't have init ?
		if(me.indexOf('loaderManager.js?init') == -1)
			return;

		//facebookSDK ?
		if(me.indexOf('facebookSDK') !== -1){
			this.addFacebookSDK();
			me = me.replace('facebookSDK', '');
		}

		var options = me.split('init')[1].replace('=', '').split(',');
		for (var b = 0; b < options.length; b++) {
			var option = options[b].split(':');
			if(option[0] && option[1]){
				if(!loaderManager.options)
					loaderManager.options = {};
				loaderManager.options[option[0]] = option[1];
			}
		}

		if(loaderManager.options)
			this.merge();
	},

	/**
	 * override default settings
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> October 19, 2013
	 */
	merge:function(){
		if(!loaderManager.options)
			return;
		var options = loaderManager.options;
		for(var setting in options){
			if(loaderManager.settings[setting])
				loaderManager.settings[setting] = options[setting];

		}
	},

	/**
	 * include jquery from javascript
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	addJquery:function(){
		this.addScript(this.cdnUrl.replace('[VERSION]', this.settings.version));
	},

	/**
	 * include facebookSDK
	 * @author Alejo JM <alejo.jm@gmail.com> October 19, 2013
	 */
	addFacebookSDK:function(){
		this.addScript("//connect.facebook.net/en_US/all.js");
	},

	/**
	 * helper method for add any script
	 * @param  {string} url
	 * @param  {boolean} async
	 * @author Alejo JM <alejo.jm@gmail.com> October 19, 2013
	 */
	addScript:function(url, async){
		var js = document.createElement('script'); js.type = 'text/javascript';
		if(async) js.async = async;
		js.src = url;
		var script = document.getElementsByTagName('script')[0];
		script.parentNode.insertBefore(js, script);
	},

	/**
	 * add link style
	 * @param  {string} url
	 * @author Alejo JM <alejo.jm@gmail.com> October 20, 2013
	 */
	addStyle:function(url){
		var css = document.createElement('link'); css.type = 'text/css';
		css.rel = 'stylesheet';
		css.href = url;
		var style = document.getElementsByTagName('link')[0];
		style.parentNode.insertBefore(css, style);
	},

	/**
	 * fire timer on wait var
	 * @return {[type]} [description]
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	timer:function  () {
		loaderManager.timeout = setTimeout(loaderManager.exists, loaderManager.settings.wait);
	},

	/**
	 * test if jQuery is added to the window, check the version
	 * @return {new timer or load}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	exists:function () {
		clearTimeout(loaderManager.timeout);

		if(!window['jQuery']){
			loaderManager.timer();
			return false;
		}
		else
		{
			if(String(window['jQuery'].fn.jquery) != loaderManager.settings.version){
				loaderManager.timer();
				return false;
			}
		}

		loaderManager.ready();
		return true;
	},

	/**
	 * get computed styles, taken from
	 * http://davidwalsh.name/vendor-prefix
	 * @return {object}
	 * @author Alejo JM <alejo.jm@gmail.com> November 06, 2013
	 */
	getVendorPrefixes : function () {
		if(!window.getComputedStyle)
			return {dom:false, lowercase:false, css:false, js:false};

		var styles = window.getComputedStyle(document.documentElement, ''),
		pre = (Array.prototype.slice
			.call(styles)
			.join('')
			.match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
			)[1],
			dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
		return {
			dom: dom,
			lowercase: pre,
			css: '-' + pre + '-',
			js: pre[0].toUpperCase() + pre.substr(1)
		};
	},

	/**
	 * this method load all javascript from one file
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	ready:function () {

		//maybe the body doesn't exists, this happend when the jquery is already included
		if(!$('body').length){
			$('body').ready(this.ready);
			return;
		}

		if(!$('#'+loaderManager.settings.id).length)
			$('body').prepend('<div id="'+loaderManager.settings.id+'"></div>');

		if(!$('#'+loaderManager.settings.id).html().length){

			$.ajaxSetup({cache:loaderManager.settings.cache});

			$('#'+loaderManager.settings.id).load(loaderManager.settings.file, function(){

				var prefixes = loaderManager.getVendorPrefixes();
				var isMoz = prefixes.lowercase && prefixes.lowercase == 'moz';

				//if we have external js, we need wait all js are loaded firefox do for us
				loaderManager.haveExternalJs();
				if(loaderManager.numberExternalJs && !isMoz)
					loaderManager.waitExternalIsLoaded();
				else
					loaderManager.allScriptsLoaded();
			});
		}
	},

	/**
	 * div javascript have externals js ?
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	haveExternalJs:function(){
		var externalJs = 0;
		var scripts = $('#'+loaderManager.settings.id+' script');
		for (var a = 0; a < scripts.length; a++) {
			if(scripts[a].src.indexOf(window.location.host) == -1 && scripts[a].src.indexOf('googleapis') == -1)
				externalJs++;
		}

		if(externalJs){
			loaderManager.numberExternalJs = externalJs;
		}

	},

	/**
	 * this method only occur when the HTML have external js
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	waitExternalIsLoaded:function(){
		var alreadyLoaded = 0;
		$(document.scripts).load(function(){
			alreadyLoaded++;
			if(alreadyLoaded >= loaderManager.numberExternalJs){
				return loaderManager.allScriptsLoaded();
			}


		});
	},

	/**
	 * Tell everyone we are ready, keep the normal behavior when window trigger the load event.
	 * Add param to the event, keep the event mark by me. Sometimes the window load event  is trigger
	 * before my load event (this happend when the loaded file finish before the window trigger the load event)
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	allScriptsLoaded:function(){
		if(!loaderManager.isReady){
			loaderManager.isReady = true;
			$(window).trigger("load", "manager");
		}

	}
};

/**
 * Jquery is ready for use ? LoaderManager will know that and more
 */
loaderManager.init();

