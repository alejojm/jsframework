

function InjectEvents (delegate){
	this.delegate = delegate;
	this.init();
}

InjectEvents.prototype.init = function() {

	for(var method in this.delegate){
		var element = this.check(method);
		if(element)
			this.add(element, method);
	}
};

InjectEvents.prototype.check = function(method) {
	var element = $('.'+method);
	if(element.length && $(element).hasClass("inject")){
		return element;
	}

	return false;
};

InjectEvents.prototype.add = function(element, method) {
	$(element).click($.proxy(this.delegate[method],this.delegate));
};

