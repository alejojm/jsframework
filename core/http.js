/**
 * Wrap the ajax call under http.get or http.post
 */
function Http(controller) {
	this.controller = controller;
	this.set();
}

Http.prototype.set = function() {
	this.success	= null;
	this.error		= null;
	this.defaults	= {showLoading: true, context:this.controller};
};

Http.prototype.get = function(options, success, dontUseCache) {
	// The url is string else object
	var setup = {};
	if (typeof options == 'string') {
		Application.loading();
		setup = $.extend(this.defaults, {url: options, success: success, context: this.controller});
	} else {
		setup = $.extend(this.defaults, options);
		if (setup.showLoading)
			Application.loading();
	}

	//we have the reponse in the cache
	var cachedResponse = localCache.get(setup.url);
	if(cachedResponse && !dontUseCache){
		Application.loadingComplete();
		return this.cachedResponse(cachedResponse, setup);
	}

	new Ajax(setup);
};

Http.prototype.post = function(options, form, success) {
	var setup = {};
	if (typeof options == 'string') {
		Application.loading();
		setup = $.extend(this.defaults, {url: options, success: success, form: form, context: this.controller});
	}	else {
		setup = $.extend(this.defaults, options);

		if (setup.showLoading)
			Application.loading();
	}

	new Ajax(setup);
};

Http.prototype.cachedResponse = function(cachedResponse, options) {
	var response = JSON.parse(cachedResponse);
	var params = options.params ? options.params : {};
	options.success.call(options.context, response, params);

    //tell everyone interested in this ajax reponse (controllerNameAjaxResponse)
    $(window).trigger(this.controller.name+'AjaxResponse', response);

	response = null;
};


Http.prototype.clear = function(name) {
	localCache.remove(name);
};

/**
 * save responses by url if is supported by the browser
 * @type   {Object}
 * @author Alejo    JM <alejo.jm@gmail.com> August 04, 2013
 */
var localCache = {

	/**
	 * if we neeed disactivate the clear feature set to false
	 * @type   {Boolean}
	 * @author Alejo     JM <alejo.jm@gmail.com> August 04, 2013
	 */
	useCleanFeature:true,

	/**
	 * every 5 minutes clear cache, or Config.cleanStorageMins
	 * @type   {Number}
	 * @author Alejo    JM <alejo.jm@gmail.com> August 04, 2013
	 */
	cleanStorageMins:5,

    /**
     * attach application listeners, onResume or onLowMemory clean entire cache
     * @return {void}
     * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
     */
    addEventListeners:function(){
		$(window).bind('Application.onPause', $.proxy(this.clear,this));
		$(window).bind('Application.onLowMemory', $.proxy(this.clear,this));
    },

	/**
	 * test if the browser support localStorage
	 * @return {true/false}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
	 */
    suported:function(){
        return window['localStorage'] !== null;
    },


    /**
     * add things to localStorage, can be string or json
     * @param  {string} name
     * @param  {string|object} value
     * @param  {boolean} notJson the value is json or not
     * @return {void}
     * @author Alejo    JM      <alejo.jm@gmail.com> August 04, 2013
     */
    add:function(name, value, notJson) {
        if(!this.suported) return false;

        //use this feature ?
        if(!Config.useLocalStorage)
            return false;

        if(!localStorage.length){
			localStorage.setItem('localCache.createdOnDate', (new Date()).toString());
        }

        if(!this.get(name)){
			var cache = !notJson ? JSON.stringify(value) : value;
			localStorage.setItem(name, cache);
        }

    },

    /**
     * get things from the localStorage, clear storage if already pass 5 minutes
     * or things setting in Config
     * @param  {string} name
     * @return {string}
     * @author Alejo    JM   <alejo.jm@gmail.com> August 04, 2013
     */
    get:function(name){
        if(!this.suported) return false;

        if(name == 'localCache.createdOnDate')
			return false;

        //use this feature ?
        if(!Config.useLocalStorage)
            return false;

		//we need to clean the cache and is activated this feature
		if(this.useCleanFeature){
			var createdOnDate = new Date(localStorage.getItem('localCache.createdOnDate'));
			var today = new Date();
			var passMinutes = Math.round((today - createdOnDate)/(1000*60));
			var clearOnMinutes = Config.cleanStorageMins ? Config.cleanStorageMins : this.cleanStorageMins;
			if(passMinutes > clearOnMinutes){
				this.clear();
				return false;
			}
		}

		return localStorage.getItem(name);
    },

    /**
     * remove things from localStorage
     * @param  {string} name
     * @return {true/false}
     * @author Alejo    JM   <alejo.jm@gmail.com> August 04, 2013
     */
    remove:function(name) {
        if(!this.suported) return false;

		if(name == 'localCache.createdOnDate')
			return false;

        return localStorage.removeItem(name);
    },

    /**
     * Clear entire localStorage
     * @return {[type]} [description]
     * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
     */
	clear:function() {
        if(!this.suported) return false;
        Log.console('localStorage.clear');
        return localStorage.clear();
    }
};

/**
 * Attach application listerners
 */
localCache.addEventListeners();

/**
 * Wrapper ajax, we need every request be a unique object
 */
function Ajax(options) {
	this.set(options);
	this.init();
}

Ajax.prototype.set = function(options) {
	this.options = options;
	this.attempts = 0;
	this.maxAttempts = 5;
	this.noInternetConnMessg = 'No tienes conexíon a internet o se encuentra intermitente. Por favor intentalo nuevamente.';
};

Ajax.prototype.init = function() {
	if (this.options.form)
		this.post();
	else
		this.get();
};

Ajax.prototype.get = function() {
	$.ajax({
		url: this.options.url,
		type: 'GET',
		dataType: 'json',
		success: this.response,
		error: this.error,
		context: this
	});
};

Ajax.prototype.post = function() {
	var data = typeof this.options.form == 'object' ? $.param(this.options.form) : $(this.options.form).serialize();
	$.ajax({
		url: this.options.url,
		type: 'POST',
		data: data,
		dataType: 'json',
		success: this.response,
		error: this.error,
		context: this
	});
	data = null;
};

Ajax.prototype.response = function(response) {
	var params = this.options.params ? this.options.params : {};

	if (this.options.showLoading)
		Application.loadingComplete();

	this.options.success.call(this.options.context, response, params);

    //tell everyone interested in this ajax reponse (controllerNameAjaxResponse)
    $(window).trigger(this.options.context.name+'AjaxResponse', response);

	//save in localStorage if is not a POST and is activated
    if(!this.options.form)
		localCache.add(this.options.url, response);

	response = null;
};

Ajax.prototype.error = function(xhr, textStatus, error) {
	if(Config.debug){
		return console.log('AJAX error:'+error+' url:'+this.options.url+' textStatus:'+textStatus);
	}

	if (this.attempts < this.maxAttempts) {
		if (Application.isAndroid() && !this.options.showLoading) {
			Application.loading();
			this.options.showLoading = true;
		}

		this.attempts++;
		new Timer({complete: this.init, context: this, duration: 2000});
	} else {
		this.attempts = 0;

		if (Application.isAndroid())
			Application.loadingComplete();

		if (confirm(this.noInternetConnMessg)) {
			if (Application.isAndroid())
				Application.loading();

			this.init();
		} else {
			// if (Application.isAndroid()) {
			if (Application.isAndroid() && Application.androidAppVersion >= "1.6.1") {
				AndroidApp.closeApplication();
			} else {
				// Load home for web browsers in mobile devices and pc
				window.location.href = "/";
			}
		}
	}
};
