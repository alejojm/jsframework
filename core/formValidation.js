/**
 * validate form on client side, using the attribute validations
 * validations="empty|email|less:Number|max:Number|etc etc etc"
 * the setting it's a object with:
 {
	error_message:true, -- add error message
	error_titlemessage:true,  -- add error title message
	error_class:'ValidationFail', -- default error class on validation fail
	hide_class:'invisible', -- default class for hide errors
	success_class:'ValidationSuccess', -- default class for success validation
	container_error:'.validation-error', -- container for errros
	message_error:null -- tghe message can have another element for show the message
}
 */
function formValidation(options) {
	if (options.form) {
		this.options = options;
		this.set();
		this.init();
	}
}

formValidation.settings = {
	error_message:true,
	error_titlemessage:true,
	hide_class:'invisible',
	error_class:'ValidationFail',
	success_class:'ValidationSuccess',
	container_error_class:'validation-error',
	message_error_class:null,
	custom_validations:{}//TODO
};

formValidation.setup = function(settings){
	formValidation.settings = jQuery.extend({}, formValidation.settings, settings);
};

formValidation.render = function() {
	var forms = jQuery('.formValidation');
	for (var a = 0; a < forms.length; a++) {
		if (!forms[a].formValidation){
			forms[a].formValidation = new formValidation({form:forms[a]});
		}
	}
	return forms;
};

formValidation.prototype.set = function(params) {
	this.form = {};
	this.formNamesValues = {};
	this.selector = 'input:text:visible, input:file:visible, input:password:visible, input:checkbox:visible, input:radio:visible, input[type="email"]:visible, input[type="number"]:visible, select:visible, textarea:visible';
	this.outselector = 'input:text:visible, input[type="number"]:visible, input[type="email"]:visible, input:password:visible, select:visible';
	this.changeselector = 'input:file:visible, textarea:visible';
	this.plain_outselector = 'input:text, input[type="number"], input[type="email"]:visible, input:password, select';
	this.plain_changeselector = 'input:file, textarea';
	this.originalErrors = {};

	this.validations = {
		email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
		numeric: /^[0-9]+$/,
		currency: /\D/g,
		empty: /^\s*$/,
		url:/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/,
		twittertag: /@([\s\S])/,
		twitter: /([\s\S])*(twitter.com)+(\/)+([\s\S])/,
		facebook: /([\s\S])*(facebook.com)+(\/)+([\s\S])/,
		pinterest: /([\s\S])*(pinterest.com)+(\/)+([\s\S])/,
		linkedin: /([\s\S])*(linkedin.com)+(\/)+([\s\S])/,
		username: /^[a-zA-Z0-9-.]+$/
	};
};

formValidation.prototype.init = function() {
	this.form = jQuery(this.options.form);
	if(this.form.get(0) && this.form.get(0).formValidationInstance){
		return this.refresh();
	}

	this.form.get(0).formValidationInstance = true;
	this.inputs = this.form.find(this.selector);
	this.getDefaultValues();
	this.addEventListeners();
	this.populateFields();

	//jquery plugin definitions
	jQuery.fn.formValidation = this;
	jQuery.fn.valueOrNumber = function(){
		if(this.get(0).validations.currency)
			return this.val().replace(/[^\d-]/g, '');
		else
			return this.val();
	};
};

formValidation.prototype.addEventListeners = function() {
	this.form.submit(jQuery.proxy(this.submit, this));
	this.form.find(this.outselector).focusout(jQuery.proxy(this.focusout, this));
	this.form.find(this.changeselector).change(jQuery.proxy(this.focusout, this));
};

formValidation.prototype.removeEventListeners = function() {
	if(!this.form.off) return;
	this.form.off('submit', jQuery.proxy(this.submit, this));
	this.form.find(this.outselector).off('focusout', jQuery.proxy(this.focusout, this));
	this.form.find(this.changeselector).off('change', jQuery.proxy(this.focusout, this));
};

formValidation.prototype.refresh = function() {
	this.removeEventListeners();
	this.inputs = this.form.find(this.selector);
	this.getDefaultValues();
	this.addEventListeners();
};

formValidation.prototype.destroy = function() {
	this.removeEventListeners();
	this.inputs = [];
	this.form = {};
	this.formNamesValues = {};
	this.originalErrors = {};
};

formValidation.prototype.getDefaultValues = function() {
	for (var a = -1, ln = this.inputs.length; ++a < ln;) {
		if(!this.inputs[a].defValue)
			this.inputs[a].defValue = this.inputs[a].value;
	}
};

formValidation.prototype.objectToForm = function(object) {
	for(var keyname in object){
		if(typeof object[keyname] == 'object')
			this.walkObject(keyname, object[keyname]);
		else
			this.formNamesValues[keyname] = object[keyname];
	}
};

formValidation.prototype.walkObject = function(keyname, object){
	for(var key in object){
		if(typeof object[key] == 'object')
			this.walkObject(keyname+'['+key+']', object[key]);
		else
			this.formNamesValues[keyname+'['+key+']'] = object[key];
	}
};

formValidation.prototype.nameToFields = function(name){
	if(name.indexOf('.') == -1)
		return name;

	var fixedName = name.replace(/\[/, '\.').replace(/]/, '\.'),
		points = fixedName.split('.'),
		fieldName = '',
		count = 0;
	for(var onename in points){
		fieldName += !count ? points[onename]+'[' : '['+points[onename]+']';
		count++;
	}

	return fieldName.replace(/\[\[/, '[');
};


formValidation.prototype.populateFields = function(data) {
	var formValidationData = this.form.find('[name="formValidationData"]');
	if(!formValidationData.length && !data)
		return;

	this.objectToForm(data ? data : JSON.parse(formValidationData.val()));
	for (var a = 0; a < this.inputs.length; a++) {
		var input = jQuery('[name="'+this.inputs[a].name+'"]');
		var fieldname = this.inputs[a].name;
		var value = this.formNamesValues[fieldname];
		if(!input.length || value === '' || typeof value == 'undefined')
			continue;
		if(typeof value == 'string' && !value.length)
			continue;
		if(typeof value == 'number' && isNaN(value))
			continue;

		if(input.is(':file'))
			continue;
		if(input.is(':radio') || input.is(':checkbox')){
			if(input.is(':checkbox') && input.filter(':checkbox').val() == value)
				input.attr('checked', true);
			if(input.is(':radio') && input.val() == value)
				input.attr('checked', true);
		}
		else
		{
			if(!input.is(':radio') && !input.is(':checkbox'))
				input.val(value);
			if(input.is('select') &&
				value !== '' &&
				!input.find('option[value="'+value+'"]').length
			)
				input.attr('option-selected', value);
		}
	}
	this.formNamesValues = {};
	formValidationData.remove();
};

formValidation.prototype.errors = function(validationErrors) {
	this.hasErrorsValidations(validationErrors);
};

formValidation.prototype.hasErrorsValidations = function(validationErrors) {
	if (typeof validationErrors !== 'object')
		return;

	// this.objectToForm(validationErrors);
	for(var fieldname in validationErrors){
		var oneFieldName = this.nameToFields(fieldname);
		this.showErrorsValidations(jQuery('[name="'+oneFieldName+'"]').get(0), validationErrors[fieldname], true);
		// break; TODO: is necesry or not ?
	}
	// this.formNamesValues = {};
};

formValidation.prototype.focusout = function(e) {
	var input = e.target;
	if (jQuery(input).is('checkbox')){
		return;
	}

	var wasValid = this.validateInput(input);

	if(!wasValid){
		this.showErrorsValidations(input);
		//fire the event this process is done
		this.form.trigger('validationFailInput', input);
	}
	else
	{
		this.showSucessValidations(input);
		//fire the event this process is done
		this.form.trigger('validationSuccessInput', input);
	}
};

formValidation.prototype.validateInput = function(input) {
	var wasValid = true;
	var validations = input.getAttribute('validations');
	if (validations) {
		if(!input.validations)
			input.validations = {};

		var methods = validations.split('|');

		if( input.defValue == input.value &&
			validations.indexOf('notrequired') !== -1){
			return wasValid;
		}

		if( input.value === "" &&
			validations.indexOf('notrequired') !== -1){
			return wasValid;
		}

		//validate method
		for (var b = 0; b < methods.length; b++) {
			var method = methods[b].split(':')[0];
			method = method == 'default' ? 'defaultValue' : method;
			if (this[method] && !this[method](input, methods[b].split(':')[1])) {
				wasValid = false;
				input.validations[method] = false;
			} else
				input.validations[method] = true;
		}
	}

	return wasValid;
};

formValidation.prototype.validate = function() {
	for (var a = 0; a < this.inputs.length; a++) {
		this.form.trigger(this.validateInput(this.inputs[a]) ? 'validationSuccessInput': 'validationFailInput', this.inputs[a]);
	}
	return !this.hasErrors();
};

formValidation.prototype.validateInputs = function(inputs, processErrors) {
	for (var a = 0; a < inputs.length; a++) {
		this.validateInput(inputs[a]);
	}

	if(processErrors)
		this.processErrors(inputs);

	return !this.hasErrors(inputs);
};

formValidation.prototype.addInputs = function(inputs) {
	this.inputs = this.inputs.add(inputs);
	inputs.filter(this.plain_outselector).focusout(jQuery.proxy(this.focusout, this));
	inputs.filter(this.plain_changeselector).change(jQuery.proxy(this.focusout, this));
};

formValidation.prototype.removeInputs = function(inputs) {
	for (var a = 0; a < inputs.length; a++) {
		for (var b = 0; b < this.inputs.length; b++) {
			if(this.inputs[b] == inputs[a]){
				this.inputs.splice(b,1);
				break;
			}
		}
	}
	inputs.filter(this.plain_outselector).off('focusout', jQuery.proxy(this.focusout, this));
	inputs.filter(this.plain_changeselector).off('change', jQuery.proxy(this.focusout, this));
};

formValidation.prototype.submit = function(e) {
	//get all inputs again
	this.inputs = this.form.find(this.selector);

	//fire the event before validate
	this.form.trigger('beforeValidation', wasValid);

	//hide all errors
	this.hideErrorsValidations();

	//validate
	var wasValid = this.validate();

	//proccess errors
	this.processErrors();

	//prevent the default event
	e.preventDefault();

	//fire the event this process is done
	this.form.trigger('validationComplete', wasValid);

	//return true/false
	return wasValid;
};

formValidation.prototype.hasErrors = function(inputs) {

	var len = !inputs ? this.inputs.length : inputs.length;
	var ins = !inputs ? this.inputs : inputs;
	for (var a = 0; a < len; a++) {
		var validations = ins[a].validations;
		for (var b in validations) {
			if (!validations[b]) {
				return true;
			}
		}
	}

	return false;
};

formValidation.prototype.processErrors = function(inputs) {
	var len = !inputs ? this.inputs.length : inputs.length;
	var ins = !inputs ? this.inputs : inputs;

	for (var a = 0; a < len; a++) {
		var validations = ins[a].validations;
		for (var b in validations) {
			if (!validations[b]) {
				this.showErrorsValidations(ins[a]);
				break;
			}
			else this.showSucessValidations(ins[a]);
		}
	}
};

formValidation.prototype.empty = function(input) {
	if (!this.isRequired(input))
		return true;
	input = jQuery(input);
	var isEmpty = input.val() === '';
	return !isEmpty;
};

formValidation.prototype.less = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) < Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.less_than = function(input, withSelector) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) < Number($(withSelector).val());
	} catch(e){ return false; }
};

formValidation.prototype.depends = function(input, withSelector) {
	if (!this.isRequired(input))
		return true;
	try{
		var dependent = $(withSelector).get(0);
		delete input.validations.depends;
		if(dependent.validations)
			delete dependent.validations.depends;
		return this.hasErrors([input, dependent]) ? false : true;
	} catch(e){ return false; }
};

formValidation.prototype.lt_datecompare = function(input, yearMonthDaySelectorsOrValue) {
	if (!this.isRequired(input))
		return true;
	try{
		var infodate = yearMonthDaySelectorsOrValue.split(',');
		var jy = jQuery(infodate[0]),
			jm = jQuery(infodate[1]),
			jd = jQuery(infodate[2]),
			jyy = jQuery(infodate[3]),
			jmm = jQuery(infodate[4]),
			jdd = jQuery(infodate[5]);
		var y  = jy.length ? Number(jy.val()) : infodate[0],
			m  = jm.length ? Number(jm.val()) : infodate[1],
			d  = jd.length ? Number(jd.val()) : infodate[2];
		var yy = jyy.length ? Number(jyy.val()) : infodate[3],
			mm = jmm.length ? Number(jmm.val()) : infodate[4],
			dd = jdd.length ? Number(jdd.val()) : infodate[5];

		if(!y || !m || !d || !yy  || !mm  || !dd )
			return true;

		return new Date(y, m, d) < new Date(yy, mm, dd);

	} catch(e){ return false; }
};

formValidation.prototype.gt_datecompare = function(input, yearMonthDaySelectorsOrValue) {
	if (!this.isRequired(input))
		return true;
	try{
		var infodate = yearMonthDaySelectorsOrValue.split(',');
		var jy = jQuery(infodate[0]),
			jm = jQuery(infodate[1]),
			jd = jQuery(infodate[2]),
			jyy = jQuery(infodate[3]),
			jmm = jQuery(infodate[4]),
			jdd = jQuery(infodate[5]);
		var y  = jy.length ? Number(jy.val()) : infodate[0],
			m  = jm.length ? Number(jm.val()) : infodate[1],
			d  = jd.length ? Number(jd.val()) : infodate[2];
		var yy = jyy.length ? Number(jyy.val()) : infodate[3],
			mm = jmm.length ? Number(jmm.val()) : infodate[4],
			dd = jdd.length ? Number(jdd.val()) : infodate[5];

		if(!y || !m || !d || !yy  || !mm  || !dd )
			return true;

		return new Date(y, m, d) > new Date(yy, mm, dd);

	} catch(e){ return false; }
};


formValidation.prototype.greater_than = function(input, withSelector) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) > Number($(withSelector).val());
	} catch(e){ return false; }
};

formValidation.prototype.max = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) <= Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.min = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) >= Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.between = function(input, range) {
	if (!this.isRequired(input))
		return true;
	try{
		var min = Number(range.split(',')[0]);
		var max = Number(range.split(',')[1]);
		var val = Number(jQuery(input).valueOrNumber());
		return (val >= min && val <= max);
	} catch(e){ return false; }

};

formValidation.prototype.equal = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return Number(jQuery(input).valueOrNumber()) == Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.digits_between = function(input, range) {
	if (!this.isRequired(input))
		return true;
	try{
		var min = Number(range.split(',')[0]);
		var max = Number(range.split(',')[1]);
		var val = jQuery(input).val().length;
		return (val >= min && val <= max);
	} catch(e){ return false; }
};

formValidation.prototype.digits_min = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return jQuery(input).val().length >= Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.regex = function(input, regex) {
	if (!this.isRequired(input))
		return true;
	try{
		return new RegExp(regex).test(jQuery(input).val());
	} catch(e){ return false; }
};

formValidation.prototype.digits_max = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return jQuery(input).val().length <= Number(stringNumber);
	} catch(e){ return false; }
};

formValidation.prototype.digits_equal = function(input, stringNumber) {
	if (!this.isRequired(input))
		return true;
	try{
		return jQuery(input).val().length == Number(stringNumber);
	} catch(e){ return false; }
};


formValidation.prototype.email = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.email.test(input.val());
};

formValidation.prototype.date = function(input) {
	if (!this.isRequired(input))
		return true;
	try{
		var date = new Date(input.value);
		if ( date.getTime && isNaN( date.getTime() ) )
			return false;
	} catch(e){ return false; }
	return true;
};

formValidation.prototype.older_than = function(input, yearsOld) {
	var birthDateString = input.value;
	try {
		var today = new Date();
		var birthDate = new Date(birthDateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age >= Number(yearsOld);
	} catch(e){ return false; }
};

formValidation.prototype.emails = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	var emails = input.val().trim().split(',');
	var emaillen = emails.length;
	var wasValid = true;
	for (var a = 0; a < emails.length; a++) {
		if(!this.validations.email.test(emails[a].trim()))
			wasValid = false;
	}
	return wasValid;
};

formValidation.prototype.numeric = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.numeric.test(input.valueOrNumber());
};

formValidation.prototype.currency = function(input) {
	if (!this.isRequired(input))
		return true;
	input = jQuery(input);
	return this.validations.numeric.test(input.val().replace(this.validations.currency, ''));
};

formValidation.prototype.url = function(input) {
	if (!this.isRequired(input))
		return true;
	input = jQuery(input);
	return this.validations.url.test(input.val());
};

formValidation.prototype.facebook = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.facebook.test(input.val());
};

formValidation.prototype.twitter = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.twitter.test(input.val());
};

formValidation.prototype.username = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.username.test(input.val());
};

formValidation.prototype.twitterusername = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return (this.validations.twitter.test(input.val()) || this.validations.twittertag.test(input.val()));
};

formValidation.prototype.twittertag = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.twittertag.test(input.val());
};

formValidation.prototype.pinterest = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.pinterest.test(input.val());
};


formValidation.prototype.linkedin = function(input) {
	if (!this.isRequired(input))
		return true;

	input = jQuery(input);
	return this.validations.linkedin.test(input.val());
};

formValidation.prototype.defaultValue = function(input) {
	return !(input.defValue == input.value);
};

formValidation.prototype.ischecked = function(input) {
	if (!this.isRequired(input))
		return true;

	return input.checked;
};

formValidation.prototype.onechecked = function(input) {
	if (!this.isRequired(input))
		return true;

	return $('input[name="'+input.name+'"]:checked').val() !== undefined;
};

formValidation.prototype.notrequired = function(input) {
	return true;
};

formValidation.prototype.isRequired = function(input) {
	var validations = input.getAttribute('validations');
	if(!validations) return true;
	var notRequired = validations.indexOf('notrequired') !== -1;
	var isDefaultValue = !this.defaultValue(input);

	input = jQuery(input);

	if(isDefaultValue && notRequired)
		return false;

	if (input.val() === '' && notRequired)
		return false;

	return true;
};

formValidation.prototype.equalto_field = function(input, selector) {
	try{
		return jQuery(this.form).find(selector).val() == jQuery(input).val();
	}
	catch(e){ return false; }

};

formValidation.prototype.getValidationElement = function(ainput) {
	var input = jQuery(ainput);
	var foundEl = false;
	var validationError;

	//the input is used for validation process
	if(input.hasClass(formValidation.settings.container_error_class)){
		validationError = input;
		foundEl = true;
	}

	//the next element is the validation
	if(!foundEl && input.next().hasClass(formValidation.settings.container_error_class)){
		validationError = input.next();
		foundEl = true;
	}

	var parent = input.parent();

	//search recursive the element
	if(!foundEl && parent.length) {
		while (!parent.is('form')) {
			validationError = parent.find('.'+formValidation.settings.container_error_class);
			if (validationError.length)
				break;
			parent = parent.parent();
		}
	}

	return validationError;
};

formValidation.prototype.getMessageErrorElement = function(ainput) {
	var input = jQuery(ainput);
	var foundEl = false;
	var validationError;

	//the input is used for validation process
	if(input.hasClass(formValidation.settings.message_error_class)){
		validationError = input;
		foundEl = true;
	}

	//the next element is the validation
	if(!foundEl && input.next().hasClass(formValidation.settings.message_error_class)){
		validationError = input.nextAll('.'+formValidation.settings.message_error_class);
		foundEl = true;
	}

	var parent = input.parent();

	//search recursive the element
	if(!foundEl && parent.length) {
		while (!parent.is('form')) {
			validationError = parent.find('.'+formValidation.settings.message_error_class);
			if (validationError.length)
				break;
			parent = parent.parent();
		}
	}

	//find if we have many errors validations by type
	var validationErrorType = null;
	var foundErrorType = null;
	if(validationError.length > 0) for (var a = 0; a < validationError.length; a++) {
		if(validationErrorType) break;
		var errorsType = validationError[a].getAttribute('validations') ? validationError[a].getAttribute('validations').split('|') : [];
		if(errorsType.length){
			foundErrorType = null;
			for(var nameError in ainput.validations){
				if(!ainput.validations[nameError]){
					foundErrorType = nameError;
					break;
				}
			}
		}
		if(foundErrorType) for (var b = 0; b < errorsType.length; b++) {
			if(errorsType[b] == foundErrorType){
				validationErrorType = $(validationError[a]);
				break;
			}
		}
	}

	validationError.addClass(formValidation.settings.hide_class);
	return validationErrorType ? validationErrorType : validationError;
};

formValidation.prototype.showErrorsValidations = function(input, message, fromServer) {
	var validationError = this.getValidationElement(input);
	if(!validationError) return;
	validationError.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class);

	//we have an input don't animate opacity, just remove invisible
	if(validationError.is('input')){
		validationError.addClass(formValidation.settings.error_class);
		return;
	}

	//the prev object is input and validationError has class input-error
	var prev = validationError.prev();
	if(validationError.hasClass("input-error") && prev.is(':input'))
		prev.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class).addClass(formValidation.settings.error_class);

	var addInnerHtml = true;
	//the message is the same input name
	if(input.name && input.name.indexOf('['+message+']') !== -1 || input.name === message){
		addInnerHtml = false;
	}

	if(addInnerHtml && formValidation.settings.message_error_class){
		var message_error = this.getMessageErrorElement(input);
		if(!message_error) return;

		//from server get only the first element
		if(fromServer){
			message_error = message_error.first();
			if(!this.originalErrors[input.name]){
				this.originalErrors[input.name] = message_error.html();
			}

		}

		if(this.originalErrors[input.name] && !fromServer){
			message_error.first().html(this.originalErrors[input.name]);
			delete this.originalErrors[input.name];
		}

		message_error.addClass(formValidation.settings.error_class)
			.css('opacity', 0)
			.removeClass(formValidation.settings.hide_class)
			.animate({opacity: 1}).html(message);
	}

	if(addInnerHtml){
		message = formValidation.settings.error_message ? message : '';
		var titlemessage = formValidation.settings.error_titlemessage ? message : '';
		if(titlemessage !== '')
			validationError.attr('title', titlemessage);
		if(message !== '')
			validationError.html(message);
		return validationError.addClass(formValidation.settings.error_class)
			.css('opacity', 0)
			.removeClass(formValidation.settings.hide_class)
			.animate({opacity: 1});
	}

	validationError.addClass(formValidation.settings.error_class).css('opacity', 0).removeClass(formValidation.settings.hide_class).animate({opacity: 1});
};

formValidation.prototype.showSucessValidations = function(input) {
	var validationError = this.getValidationElement(input);
	if(!validationError) return;
	validationError.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class);

	//we have an input don't animate opacity, just remove invisible
	if(validationError.is('input')){
		validationError.addClass(formValidation.settings.success_class);
		return;
	}

	//the prev object is input and validationError has class input-error
	var prev = validationError.prev();
	if(validationError.hasClass("input-error") && prev.is(':input')){
		prev.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class).addClass(formValidation.settings.success_class);
		return validationError.addClass(formValidation.settings.hide_class);
	}

	validationError.addClass(formValidation.settings.success_class).removeClass(formValidation.settings.hide_class);
	if(formValidation.settings.message_error_class){
		this.getMessageErrorElement(input)
			.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class)
			.addClass(formValidation.settings.hide_class);
	}
};

formValidation.prototype.hideErrorsValidations = function() {
	this.inputs.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class);
	if(!jQuery(formValidation.settings.container_error_class).is('input'))
		jQuery(formValidation.settings.container_error_class).addClass(formValidation.settings.hide_class);

	if(formValidation.settings.message_error_class){
		jQuery(formValidation.settings.message_error_class)
			.removeClass(formValidation.settings.error_class+' '+formValidation.settings.success_class)
			.addClass(formValidation.settings.hide_class);
	}
};

/**
 * Find all elements that have class formValidation and fire
 */
jQuery(document).ready(function() { formValidation.render();});