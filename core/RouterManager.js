/*!
 * RouterManager parse routes array and listen hashchange event
 * Copyright (c) Alejo JM <alejo.jm@gmail.com> 2013-2016
 * @version 1.0.11
 *
 * Free to use under terms of MIT license or new BSD license.
 *
 */
(function(global){
	"use strict";

	var RouterManager = {
		/**
		 * routes having all information path, controller, template
		 * @type   {Array}
		 */
		routes:[],

		/**
		 * the current currentController we find on routeBefore callback
		 * @type   {[type]}
		 */
		currentController:null,

		/**
		 * preview controller on animation view
		 * @type   {Object}
		 */
		prevController:null,

		/**
		 * the current route we find on routeMatched callback
		 * @type   {[type]}
		 */
		currentRoute:null,

		/**
		 * next route
		 * @type   {object}
		 */
		nextRoute:null,

		/**
		 * className for add to the view in the animation process
		 * @type   {string}
		 */
		className:null,

		/**
		 * get prefix browser for event
		 * @type   {object}
		 */
		event:{},

		/**
		 * if we need animations processing
		 * @type   {boolean}
		 */
		animated:false,

		/**
		 * if not found route we will take the browser to that path
		 * @type   {String}
		 */
		notFoundRoute:null,

		/**
		 * complete notfound Url
		 * @type   {[type]}
		 */
		notFoundUrl:null,

		/**
		 * body element
		 * @type   {HTMLBody}
		 */
		body:null,

		/**
		 * the direction of the animation
		 * @type   {string} forward or backward
		 */
		direction:null,

		/**
		 * last executed location.hash
		 * @type {String}
		 */
		lastPath:'',

		/**
		 * backbutton is enabled for android
		 * @type {Boolean}
		 */
		canback:true,

		/**
		 * constructor add hashchange event if is available and parse routes, add cordova events
		 * @param  {array} routes array({path:somepath, controller:function(){}, template:string})
		 * @return {void}
		 */
		init:function(routes, animated){

			if (!('onhashchange' in window))
				return;

			RouterManager.animated = animated;
			RouterManager.routes = routes;
			RouterManager.body = document.querySelector('body');

			//setup default animation for routes
			for (var a = 0; a < RouterManager.routes.length; a++) {
				if(typeof RouterManager.routes[a].animated === 'undefined')
					RouterManager.routes[a].animated = RouterManager.animated;
			}

			if(RouterManager.animated)
				RouterManager.getPrefix();

			//has change event
			window.addEventListener('hashchange', RouterManager.onhashchange, false);

			//init the route
			RouterManager.routeBefore();

			//events triggered by cordova
			document.addEventListener("pause", RouterManager.unloadCurrentRoute, false);
			document.addEventListener("resume", RouterManager.reloadCurrentRoute, false);
			document.addEventListener("backbutton", RouterManager.backbutton, false);
		},

		/**
		 * haschange find next route
		 * @param  {Event} event
		 * @return {void}
		 */
		onhashchange:function (event) {
			if(RouterManager.currentRoute)
				RouterManager.routeAfter();
			else
				RouterManager.routeBefore();
		},

		/**
		 * the app is paused call controller destroy method and remove the view
		 * @return {void}
		 */
		unloadCurrentRoute:function(){
			if(RouterManager.currentRoute && RouterManager.currentRoute.animated)
				RouterManager.controllerMethod('viewLeave', RouterManager.currentController);
			RouterManager.controllerMethod('destroy', RouterManager.currentController);
			document.querySelector('#view_content').innerHTML = '';
			RouterManager.currentController = null;
			RouterManager.currentRoute = null;
		},

		/**
		 * aplication is on resume call instance the controller and call necessary methods
		 * @return {void}
		 */
		reloadCurrentRoute:function(){
			var route = RouterManager.findRoute();
			RouterManager.currentController = new route.controller(route.params);
			RouterManager.controllerMethod('init', RouterManager.currentController, route);
			document.querySelector('#view_content').innerHTML = route.template;
			RouterManager.controllerMethod('viewAdded', RouterManager.currentController, route);
			RouterManager.currentRoute = route;
		},

		/**
		 * backbutton
		 * @param  {void} event
		 * @return {void}
		 */
		backbutton:function (event) {
			if(!RouterManager.canback)
				return event.preventDefault();
			history.back();
		},

		/**
		 * enable back button
		 * @return {void}
		 */
		enableBack:function(){
			RouterManager.canback = true;
		},

		/**
		 * disable backbutton
		 * @return {void}
		 */
		disableBack:function(){
			RouterManager.canback = false;
		},

		/**
		 * reload current route, remove controller and instance again
		 * @return {void}
		 */
		reload:function(){
			RouterManager.unloadCurrentRoute();
			RouterManager.reloadCurrentRoute();
		},

		/**
		 * get start and end events names by browser
		 * @return {void}
		 */
		getPrefix:function(){
			var el = document.querySelector('#view_content');
			if(!el) throw new Error('missing div with id #view_content, RouterManager error');

			var transitions = {
				'WebkitTransition':{name:'webkit',start:'webkitAnimationStart', end:'webkitAnimationEnd', iteration:'webkitAnimationIteration', transitionend:'webkitTransitionEnd', css:'-webkit-'},
				'MozTransition':{name:'Moz',start:'animationstart', end:'animationend', iteration:'animationiteration', transitionend:'transitionend', css:'-moz-'},
				'OTransition':{name:'O',start:'oanimationstart', end:'oanimationend', iteration:'oanimationiteration', transitionend:'oTransitionEnd', css:'-o-'},
				'transition':{name:'ms',start:'MSAnimationStart', end:'MSAnimationEnd', iteration:'MSAnimationIteration', transitionend:'transitionend', css:''}
			};

			for(var t in transitions){
				if( el.style[t] !== undefined ){
					RouterManager.event = transitions[t];
					return ;
				}
			}
		},

		/**
		 * dispatch event shortcut
		 * @param  {string} name
		 * @return {void}
		 */
		dispatchEvent:function(name, controller, route){
			var event = document.createEvent('Event');
			event.initEvent(name, true, true);
			if(controller)
				event.controller = controller;
			if(route)
				event.route = route;
			document.dispatchEvent(event);
		},

		/**
		 * test method exists and call
		 * @param  {string} name
		 * @param  {object} controller
		 * @return {void}
		 */
		controllerMethod:function(name, controller, route, params){
			var result = true;
			if(name in controller){
				result = controller[name](params ? params : RouterManager.direction);
			}
			RouterManager.dispatchEvent('route'+name.charAt(0).toUpperCase() + name.slice(1), controller, route);
			return typeof result === 'undefined' ? true : result;
		},

		/**
		 * find route using location hash and routes definitions
		 * @return {object}
		 */
		findRoute:function(){

			var path = location.hash.replace('#', '');
			var pathInfo = path.split('/');
			var notFoundPath;
			var params;

			for (var a = 0; a < RouterManager.routes.length; a++) {
				RouterManager.routes[a].params = {};

				if( path == RouterManager.routes[a].path){
					return RouterManager.routes[a];
				}

				if(!RouterManager.notFoundRoute && RouterManager.routes[a].notFound){
					RouterManager.notFoundRoute = RouterManager.routes[a];
				}

				if(!RouterManager.routes[a].pathRegExp)
					RouterManager.routes[a].pathRegExp = RouterManager.pathRegExp(RouterManager.routes[a].path, RouterManager.routes[a]);
				var found = RouterManager.matchRoute(path, RouterManager.routes[a].pathRegExp);
				if(found){
					RouterManager.routes[a].params = found;
					return RouterManager.routes[a];
				}
			}

			RouterManager.redirectNotFound();
			return false;
		},

		/**
		 * @autor https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js
		 * @param path {string} path
		 * @param opts {Object} options
		 * @return {?Object}
		 *
		 * @description
		 * Normalizes the given path, returning a regular expression
		 * and the original path.
		 *
		 * Inspired by pathRexp in visionmedia/express/lib/utils.js.
		 */
		pathRegExp:function (path, opts) {
			var insensitive = opts.caseInsensitiveMatch,
			ret = {
			originalPath: path,
			regexp: path
			},
			keys = ret.keys = [];

			path = path
			.replace(/([().])/g, '\\$1')
			.replace(/(\/)?:(\w+)(\*\?|[\?\*])?/g, function(_, slash, key, option) {
				var optional = (option === '?' || option === '*?') ? '?' : null;
				var star = (option === '*' || option === '*?') ? '*' : null;
				keys.push({ name: key, optional: !!optional });
				slash = slash || '';
				return ''
					+ (optional ? '' : slash)
					+ '(?:'
					+ (optional ? slash : '')
					+ (star && '(.+?)' || '([^/]+)')
					+ (optional || '')
					+ ')'
					+ (optional || '');
				}
			)
			.replace(/([\/$\*])/g, '\\$1');

			ret.regexp = new RegExp('^' + path + '$', insensitive ? 'i' : '');
			return ret;
		},

		/**
		 * @autor https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js
		 * @param on {string} current url
		 * @param route {Object} route regexp to match the url against
		 * @return {?Object}
		 *
		 * @description
		 * Check if the route matches the current url.
		 *
		 * Inspired by match in
		 * visionmedia/express/lib/router/router.js.
		 */
		matchRoute:function (on, route) {
			var keys = route.keys,
			params = {};

			if (!route.regexp) return null;

			var m = route.regexp.exec(on);
			if (!m) return null;

			for (var i = 1, len = m.length; i < len; ++i) {
				var key = keys[i - 1];

				var val = m[i];

				if (key && val) {
					params[key.name] = val;
				}
			}
			return params;
		},

		/**
		 * load templateURL and execute callback
		 * @param  {Object}   route
		 * @param  {Function} callback
		 * @return {void}
		 */
		loadTemplate:function(route, callback){
			RouterManager.controllerMethod('viewLoading', RouterManager.currentController);
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4) {
					xhr.onreadystatechange = null;
					route.template = xhr.responseText;
					setTimeout(function() {
						callback.call(RouterManager, route);
					});
				}
			};
			xhr.open("GET", route.templateUrl, true);
			xhr.send();
		},

		/**
		 * test if the controller is a string and we have requirejs for load
		 * @param  {object}   route
		 * @param  {Function} callback
		 * @param  {Mixed} params
		 * @return {boolean}
		 */
		loadController:function(route, callback, params){
			if(typeof route.controller == 'string' && 'requirejs' in window){
				require([route.controller], function(loadedController){
					if(!loadedController)
						throw new Error('missing define for controller, RouterManager error (http://requirejs.org/docs/api.html#funcmodule)');
					route.controller = loadedController;
					callback.call(RouterManager, params);
				});
				return true;
			}
			return false;
		},

		/**
		 * redirect the browser to notfound path
		 * @return {void}
		 */
		redirectNotFound:function(){
			if(!RouterManager.notFoundRoute)
				return;
			RouterManager.nextRoute = RouterManager.notFoundRoute;
			location.hash = RouterManager.nextRoute.path;
		},

		/**
		 * find the current route, if the controller on init return false stop execution
		 * setup the currentController property
		 * @return {void}
		 */
		routeBefore:function(routeParam){
			if(RouterManager.currentRoute)
				return;
			var route = !routeParam ? RouterManager.findRoute() : routeParam;
			if(route){
				if(RouterManager.loadController(route, RouterManager.routeBefore, route))
					return;
				RouterManager.currentController = new route.controller(route.params);
				if(!RouterManager.controllerMethod('init', RouterManager.currentController, route))
					return RouterManager.unloadCurrentRoute();
				if(route.templateUrl && !route.template)
					return RouterManager.loadTemplate(route, RouterManager.addTemplate);
				if(route.templateUrl && route.template){
					RouterManager.controllerMethod('viewLoading', RouterManager.currentController);
					RouterManager.addTemplate(route);
				}
			}
		},

		/**
		 * swap route with no animation
		 * @param  {Object} nextRoute
		 * @return {void}
		 */
		swapRoute:function(nextRoute){
			RouterManager.controllerMethod('destroy', RouterManager.currentController);
			if(RouterManager.loadController(nextRoute, RouterManager.swapRoute, nextRoute))
				return;
			RouterManager.currentController = new nextRoute.controller(nextRoute.params);
			if(!RouterManager.controllerMethod('init', RouterManager.currentController, nextRoute))
				return RouterManager.unloadCurrentRoute();
			if(nextRoute.templateUrl && !nextRoute.template)
				return RouterManager.loadTemplate(nextRoute, RouterManager.addTemplate);
			if(nextRoute.templateUrl && nextRoute.template){
				RouterManager.controllerMethod('viewLoading', RouterManager.currentController);
				RouterManager.addTemplate(nextRoute);
			}
		},

		/**
		 * addView template inform to the controller
		 * @param  {Object} route
		 * @return {void}
		 */
		addTemplate:function(route){
			var params = {template:route.template, addView:RouterManager.refreshTemplate};
			var template = RouterManager.controllerMethod('beforeAddView', RouterManager.currentController, route, params);
			if(template){
				document.querySelector('#view_content').innerHTML = typeof template == 'string' ? template : route.template;
				RouterManager.controllerMethod('viewAdded', RouterManager.currentController, route);
			}
			RouterManager.currentRoute = route;
		},

		/**
		 * callback method when the user manipulates the view using beforeAddView
		 * the user most execute this callback keeping the correct flow
		 * @param  {string} stringView string with html inside
		 * @return {void}
		 */
		refreshTemplate:function(stringView){
			document.querySelector('#view_content').innerHTML = stringView;
			RouterManager.controllerMethod('viewAdded', RouterManager.currentController, RouterManager.currentRoute);
		},

		/**
		 * find the current route and the next, add templates views and classes for the animation
		 * when some controller on init return false the current route will the same, don't call swap route
		 * @return {void}
		 */
		routeAfter:function(){
			var nextRoute = RouterManager.findRoute();
			if(!nextRoute)
				return;

			if(!nextRoute.animated || !RouterManager.animated){
				return RouterManager.swapRoute(nextRoute);
			}
			RouterManager.swapRouteAnim(nextRoute);
		},

		/**
		 * swap route with animation enabled
		 * @param  {Object} nextRoute
		 * @return {void}
		 */
		swapRouteAnim:function(nextRoute){
			//the next controller will stop the route ?
			if(RouterManager.loadController(nextRoute, RouterManager.swapRouteAnim, nextRoute))
				return;
			var nextController = new nextRoute.controller(nextRoute.params);
			if(!RouterManager.controllerMethod('init', nextController, nextRoute))
				return RouterManager.unloadCurrentRoute();

			RouterManager.nextRoute = nextRoute;
			var activeRoute = RouterManager.currentRoute;

			if(RouterManager.nextRoute && activeRoute){
				RouterManager.className = RouterManager.direction = RouterManager.nextRoute.path.length <= activeRoute.path.length ? 'backward' : 'forward';
			}

			if(!RouterManager.className)
				return;

			//nextRoute have specific className
			if(RouterManager.nextRoute.animated[RouterManager.className])
				RouterManager.className = RouterManager.nextRoute.animated[RouterManager.className];

			RouterManager.prevController = RouterManager.currentController;
			var leaveView = document.querySelector('#view_content').children[0];
			RouterManager.controllerMethod('viewLeave', RouterManager.currentController);
			leaveView.addEventListener(RouterManager.event.start, RouterManager.animationLeaveStart, false);

			RouterManager.currentController = null;
			RouterManager.currentController = nextController;
			if(RouterManager.nextRoute.templateUrl && !RouterManager.nextRoute.template)
				return RouterManager.loadTemplate(RouterManager.nextRoute, RouterManager.swapRouteAnimReady);
			if(RouterManager.nextRoute.templateUrl && RouterManager.nextRoute.template)
				RouterManager.controllerMethod('viewLoading', RouterManager.currentController);
			setTimeout(function() {
				RouterManager.swapRouteAnimReady(RouterManager.nextRoute);
			});
		},

		/**
		 * continue with swap animated route beforeload template or not
		 * @return {void}
		 */
		swapRouteAnimReady:function(route){
			var leaveView = document.querySelector('#view_content').children[0];
			var viewContent = document.querySelector('#view_content');
			viewContent.insertAdjacentHTML('beforeend', RouterManager.nextRoute.template);

			var enterView = viewContent.children[viewContent.children.length-1];
			RouterManager.controllerMethod('viewAdded', RouterManager.currentController, route);
			enterView.addEventListener(RouterManager.event.start, RouterManager.animationEnterStart, false);

			RouterManager.body.classList.add('rm-state-animate');

			//forward first add class to the enter view
			var leaveTime, enterTime;
			if(RouterManager.direction == 'forward'){
				enterView.classList.add('rm-animate');
				enterView.classList.add(RouterManager.className);
				enterView.classList.add('enter');
				leaveView.classList.add('rm-animate');
				leaveView.classList.add(RouterManager.className);
				leaveView.classList.add('leave');
				enterTime = document.defaultView.getComputedStyle(enterView, "")[RouterManager.event.name+'AnimationDuration'].replace('s', '');
				leaveTime = document.defaultView.getComputedStyle(leaveView, "")[RouterManager.event.name+'AnimationDuration'].replace('s', '');
			} else {
				leaveView.classList.add('rm-animate');
				leaveView.classList.add(RouterManager.className);
				leaveView.classList.add('leave');
				enterView.classList.add('rm-animate');
				enterView.classList.add(RouterManager.className);
				enterView.classList.add('enter');
				leaveTime = document.defaultView.getComputedStyle(leaveView, "")[RouterManager.event.name+'AnimationDuration'].replace('s', '');
				enterTime = document.defaultView.getComputedStyle(enterView, "")[RouterManager.event.name+'AnimationDuration'].replace('s', '');
			}

			//sometimes in the device animationEnd is not called, use timeout ensure leave is always called
			setTimeout(RouterManager.animationEnterEnd, (Number(enterTime)*1000));
			setTimeout(RouterManager.animationLeaveEnd, (Number(leaveTime)*1000));

			viewContent = leaveView = enterView = null;
		},

		/**
		 * inform to the controller what is happening and fire global events on window
		 * @param  {event} event
		 * @return {void}       [description]
		 */
		animationEnterStart:function(event){
			RouterManager.controllerMethod('viewEnterStart', RouterManager.currentController);
			event.target.removeEventListener(RouterManager.event.start, RouterManager.animationEnterStart);
			RouterManager.body.classList.add(RouterManager.direction+'-enter');
		},

		/**
		 * inform to the controller what is happening and fire global events on window
		 * @return {void}       [description]
		 */
		animationEnterEnd:function(){
			RouterManager.controllerMethod('viewEnterEnd', RouterManager.currentController);
			RouterManager.body.classList.remove(RouterManager.direction+'-enter');
		},

		/**
		 * inform to the controller what is happening and fire global events on window
		 * @return {void}       [description]
		 */
		animationLeaveStart:function(event){
			RouterManager.controllerMethod('viewLeaveStart', RouterManager.prevController);
			event.target.removeEventListener(RouterManager.event.start, RouterManager.animationLeaveStart);
			RouterManager.body.classList.add(RouterManager.direction+'-leave');
		},

		/**
		 * the animation end remove the first children and clean the className
		 * @param  {Event} event
		 * @return {void}
		 */
		animationLeaveEnd:function(){
			var view_content = document.querySelector('#view_content');
			RouterManager.controllerMethod('destroy', RouterManager.prevController);
			view_content.removeChild(view_content.children[0]);
			view_content.children[0].classList.remove('rm-animate');
			view_content.children[0].classList.remove(RouterManager.className);
			view_content.children[0].classList.remove('enter');
			RouterManager.body.classList.remove(RouterManager.direction+'-leave');
			RouterManager.body.classList.remove('rm-state-animate');
			view_content = null;
			RouterManager.currentRoute = RouterManager.nextRoute;
		}
	};

	/**
	 * public Api
	 * @type {Object}
	 */
	var RouterManagerApi = {
		init:RouterManager.init,
		reload:RouterManager.reload,
		enableBack:RouterManager.enableBack,
		disableBack:RouterManager.disableBack
	};

	/**
	 * expose RouterManagerApi object
	 */
	if (typeof module !== "undefined" && module.exports) {
		module.exports = RouterManagerApi;
	} else if (typeof define === "function" && define.amd) {
		define(function(){return RouterManagerApi;});
	} else {
		global.RouterManager = RouterManagerApi;
	}

})(this);