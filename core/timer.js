/**
* Timer object used for wait and execute some callback
*/
function Timer(options) {
	this.options = options;
	this.timeOut = null;
	this.init();
}

Timer.prototype.init = function() {
	var appTimer = window['Application'] && Application.timer ? Application.timer : 500;
	var duration = this.options.duration ? this.options.duration : appTimer;
	this.timeout = setTimeout($.proxy(this.completeTimer, this), duration);
};

Timer.prototype.completeTimer = function() {
	clearTimeout(this.timeOut);
	this.options.complete.call(this.options.context);
};
