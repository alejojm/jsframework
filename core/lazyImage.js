/**
* Load images based on class lazyImage, data have the real url for the image
* only works with the events load, ajaxResponse, viewDidAppear  events
*/

$(window).on('load', function(event){
	new lazyImage();
});

function lazyImage (){
	this.set();
	this.init();
}

lazyImage.render = function(){
	if($('.lazyImage').length)
		new lazyImage();
};

lazyImage.prototype.set = function() {
	this.images = null;
	this.target = null;
};

lazyImage.prototype.init = function() {
	this.getImages();
	this.loadImages(false);
	this.addEventListeners();
};

lazyImage.prototype.addEventListeners = function() {
	$(window).on('scrollstop', $.proxy(this.scrollstop,this));
};

lazyImage.prototype.scrollstop = function(e) {
	this.getImages();
	this.loadImages(true);
};

lazyImage.prototype.getImages = function() {
	this.images = $('.lazyImage');
	if(!this.images.length){
		$(window).off('scrollstop', $.proxy(this.scrollstop,this));
		this.images = null;
	}
};

lazyImage.prototype.loadImages = function(scrollstop) {
	if(this.images && this.images.length) for (var a=0; a < this.images.length; a++) {
		if(this.isScrolledIntoView(this.images[a], scrollstop)){
			var div = $(this.images[a]);
			var image = new Image();
			image.src = div.attr("data");
			image.delegate = div;
			$(image).load($.proxy(this.loaded,this));
		}
	}
};

lazyImage.prototype.loaded = function(e) {
	e.target.className = 'lazyImageLoaded';
	var div = e.target.delegate;
	$(e.target).insertBefore(div);
	div.remove();
};


lazyImage.prototype.isScrolledIntoView = function(elem, scrollstop)
{
	var win = !scrollstop && window.parent ? $(window.parent) : $(window);
	var el = $(elem);

    var docViewTop = win.scrollTop();
    var docViewBottom = docViewTop + win.height();
    var elemTop = el.offset().top;
    var elemBottom = elemTop + el.height();
	return (docViewBottom >= elemTop && docViewTop <= elemBottom);
};
