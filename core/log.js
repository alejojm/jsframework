/**
 * Log the aplication using firebug
 */
var Log = {
	console:function(message) {
		if (window.console && Config.debug) {
			window.console.log(message);
		}
	},

	warn:function(message) {
		if (window.console) {
			window.console.warn(message);
		}
	}

};
