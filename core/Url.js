var Url = {
	proto: window.location.protocol,
	host: window.location.host,
	path: window.location.pathname,
	hash: window.location.hash,
	files: { php: 'php', html: 'html', aspx: 'apsx', asp: 'asp' },


	/**
	* Return the current url http://hostname.some/
	*/
	base: function() {
		return this.proto + '//' + this.host;
	},

	/**
	 * get the ?
	 * @return {string}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
	 */
	getWebRoot: function() {
		var lastSlash = this.path.lastIndexOf('/') + 1;
		var webroot = this.path.substr(0, lastSlash);
		this.webroot = webroot;
	},

	/**
	 * get url var from queryString
	 * @param  {string} key
	 * @param  {string} queryString
	 * @return {mixed(string|boolean)}
	 * @author Alejo    JM          <alejo.jm@gmail.com> August 04, 2013
	 */
	getUrlVar: function(key, queryString) {
		var qs = !queryString ? window.location.search : queryString;
		qs = qs.replace('?', '&');
		var spSearch = qs.split('&');
		var allVars = false;
		var queryVars = {};
		for (var a = 0; a < spSearch.length; a++) {
			var get = spSearch[a].split('=');
			if(!key){
				allVars = true;
				if(get[0])
					queryVars[get[0]] = get[1];
			}
			else{
				if(get[0] && get[0] == key){
					if (get[1])
						return get[1];
					else
						return true;
				}
			}

		}

		if(allVars)
			return queryVars;
		else
			return false;
	},

	/**
	 * get url var from window.location.hash
	 * @param  {string} key
	 * @return {mixed(string|boolean)}
	 * @author Alejo    JM          <alejo.jm@gmail.com> August 04, 2013
	 */
	getHashVar: function(key) {
		var qs = window.location.hash;
		qs = qs.replace('?', '&');
		var spSearch = qs.split('&');

		for (var a in spSearch) {
			var get = spSearch[a].split('=');

			if (get.length > 1 && get[0] == key) {
				return get[1];
			}
		}

		return false;
	},

	/**
	 * get window.location.search
	 * @param  {string} type remove from the search the char "?"
	 * @return {string}
	 * @author Alejo    JM   <alejo.jm@gmail.com> August 04, 2013
	 */
	getUrlVars: function(type) {
		var qs = window.location.search;

		if(qs === '')
			return '';

		if(type == 'string')
			return qs.replace('?','');
	},

	/**
	 * get the file in the url
	 * @return {mixed(boolean|string)} [description]
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
	 */
	getUrlFile: function() {
		var qs = window.location.pathname;

		if (qs == '/')
			return false;

		var spSearch = qs.split('/');
		for(var a in spSearch) {
			var ext = spSearch[a].split('.');

			if (this.files[ext[1]]) {
				return spSearch[a];
			}
		}

		return false;
	},

	/**
	 * Get controller in the hash, first one
	 * @return {string}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 02, 2013
	 */
	getHashController:function(someHash){
		var hash = someHash ? someHash.replace('#', '') : window.location.hash.replace('#', '');
		hash = this.removeUrlVars(hash);

		if(hash.length > 0){
			if(hash.indexOf('/') != -1){
				var pieces = hash.split('/');
				return pieces[0];
			}
			else
				return hash;
		}
		return false;
	},

	/**
	 * get the method in the hash
	 * @return {string} [description]
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 02, 2013
	 */
	getHashMethod:function(someHash){
		var hash = someHash ? someHash.replace('#', '') : window.location.hash.replace('#', '');
		hash = this.removeUrlVars(hash);
		if(hash.length > 0){
			if(hash.indexOf('/') != -1){
				var pieces = hash.split('/');
				return pieces[1] ? pieces[1] : false;
			}
		}
		return false;
	},

	/**
	 * get params in the hash
	 * @return {array}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 02, 2013
	 */
	getHashParams:function(someHash){
		var hash = someHash ? someHash.replace('#', '') : window.location.hash.replace('#', '');
		hash = this.removeUrlVars(hash);
		var controller = this.getHashController();
		var method = this.getHashMethod();
		var params = hash.replace(controller, '').replace(method, '');

		if(!params || params === '' || params === '/')
			return false;
		params = params.replace('//', '');
		if(params.charAt( params.length-1 ) == "/")
			params = params.slice(0, -1);

		params = params.split('/');
		return params;
	},

	/**
	 * get controller, method and params in the hash
	 * @return {object}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 02, 2013
	 */
	getHashOptions:function(someHash){
		var controller = this.getHashController(someHash);
		var method = this.getHashMethod(someHash);
		var params = this.getHashParams(someHash);
		return {controller:controller, method:method, params:params};
	},

	/**
	 * add to the hash url some var
	 * @param  {string} name
	 * @param  {string} value
	 * @author Alejo JM <alejo.jm@gmail.com> December 03, 2013
	 */
	setUrlVar:function(name, value){
		var and = window.location.hash.indexOf('?') == -1 ? '?' : '&';
		var exists = this.getUrlVar(name, window.location.hash);
		if(exists){
			var newhash = '';
			if(window.location.hash.indexOf('?'+name) !== -1){
				newhash = window.location.hash.replace('?'+name+'='+exists, '');
				window.location.hash = newhash+'?'+name+'='+value;
			}

			if(window.location.hash.indexOf('&'+name) !== -1){
				newhash = window.location.hash.replace('&'+name+'='+exists, '');
				window.location.hash = newhash+'&'+name+'='+value;
			}

		}else{
			window.location.hash += and+name+'='+value;
		}

	},

	removeUrlVars:function(someUrlString){
		var initPos = someUrlString.indexOf('?');
		var cleanUrl = someUrlString.substr(0, initPos);
		if(cleanUrl === "")
			return someUrlString;
		return cleanUrl;
	},

	/**
	 * encode url
	 * @param  {string} str
	 * @return {string}     can passed to the browser
	 * @author Alejo    JM  <alejo.jm@gmail.com> August 04, 2013
	 */
	urlencode: function(str) {
		str = (str+'').toString();
		return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
	},

	/**
	 * url decode to the string params
	 * @param  {[type]} str [description]
	 * @return {[type]}     [description]
	 * @author Alejo    JM  <alejo.jm@gmail.com> August 04, 2013
	 */
	urldecode: function(str) {
		return decodeURIComponent((str + '').replace(/\+/g, '%20'));
	},

	/**
	 * get cakestyle vars var:value
	 * @param  {string} key
	 * @param  {string} queryString use this one or the pathname
	 * @return {mixed(string|boolean)}
	 * @author Alejo    JM          <alejo.jm@gmail.com> August 04, 2013
	 */
	getUrlNamed: function(key, queryString) {
		var qs = !queryString ? window.location.pathname : queryString;
		var spSearch = qs.split('/');
		for (var a in spSearch) {
			var get = spSearch[a].split(':');

			if (get.length > 1 && get[0]==key) {
				return get[1];
			}
		}

		return false;
	},

	/**
	 * create timestamp
	 * @return {int}
	 * @author Alejo    JM            <alejo.jm@gmail.com> August 04, 2013
	 */
	time: function() {
		return Math.floor(new Date().getTime() / 1000);
	},

	/**
	 * open new window based on options
	 * @param  {object} options {url:/some/url, width:(optional), height:(optional)}
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> March 26, 2014
	 */
	open:function(options){
		if(!options.url) return;
		var width  = options.width ? options.width : 800;
		var height = options.height ? options.height : 600;
		var left   = (screen.width/2)-(width/2);
		var top    = (screen.height/2)-(height/2);
		var name   = options.name ? options.name : null;
		window.open(options.url, name, 'scrollbars=yes,status=no,location=no,resizable=no,width='+width+', height='+height+', top='+top+', left='+left);
	}

};
