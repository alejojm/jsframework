var Config = {
	/**
	 * Debug State
	 */

	/**
	 * we are debuging the app
	 * [Development true | production false ]
	 * @type {String}
	 */
	debug:true,

	/**
	 * where the service live
	 * @type   {String}
	 */
	host : "http://bonoteca.ajm",

	/**
	 * Analytics account to track with
	 * @type {String}
	 */
	analytics_account:'UA-8723930-51',

	/**
	 * log what url we will track
	 * [Development true | production false ]
	 * @type {Boolean}
	 */
	log_analytics:true,

	/**
	 * send the tracking information to Analytics servers
	 * [Development false | production true ]
	 * @type {Boolean}
	 */
	track_analytics:false,

	/**
	 * save things in localStorage ?
	 * [Development false | production true or commented ]
	 * @type   {Boolean}
	 */
	useLocalStorage:false,

	/**
	 * default number to call
	 */
	contact_phone : 3188009845,

	/**
	 * what version we are running
	 */
	version : "1.7",

	/**
	 * querystring to add on load external resources
	 */
	qs : "?version=[VERSION]",

	/**
	 * where the LaBonoteca live in the playstore
	 */
	google_play_url : "http://play.google.com/store/apps/details?id=com.evo.labonoteca",

	/**
	 * novedades url when the user have account
	 */
	url_novedad_ciudad : "/webservice/novedades/ciudad/[CITY_ID]/nuevos/android",

	/**
	 * novedad url when the use dont have account
	 */
	url_primera_vez : "primeravez.html",

	/**
	 * facebook app id
	 */
	facebook_appId:458589804173287,

	/**
	 * redirect uri to send on facebook login
	 */
	redirect_uri : "[DOMAIN]/mobile/facebook/index/?from=[FROM]",

	/**
	 * login on facebook
	 */
	url_facebook_login : 'https://[WWW].facebook.com/dialog/oauth/?client_id=[FACEBOOK_APP_ID]&redirect_uri=[REDIRECT_URI]&scope=email,user_birthday,publish_actions'
};
