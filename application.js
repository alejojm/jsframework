/**
 * Wrap entire Application
 * @type {Object}
 */
var Application = {
	/**
	 * HTML to wrap all events will trigger
	 */
	context: 'body',

	/**
	 * Time for wait used by Timer Object
	 */
	timer: 300,

	/**
	 * Indicates the applications is requesting things from the server
	 */
	request: false,

	/**
	 * Is the version for android app
	 */
	androidAppVersion: "",

	/**
	 * Controller stack, every controller will register himself here
	 */
	controllers:{},

	/**
	 * Events are registered here for all App
	 */
	events:{},

	/**
	 * this is the first controller loaded, is the start point
	 * to override this value, in your controller firt line add Application.main = "YourMainController"
	 * @type   {String}
	 * @author Alejo    JM <alejo.jm@gmail.com> August 03, 2013
	 */
	main: 'AppController',

	/**
	 * global init function load, init the first controller
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com>
	 */
	init:function(e){

		if(!window[this.main])
			throw new Error("Application don't find "+this.main+" to initialize");

		//instance the appController, he know what to do with the current App
		this.controllers[this.main] = new window[this.main]();
	},

	/**
	 * Define if visitor become from mobile device.
	 */
	isMobile: function() {
		return (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|Windows Phone|IEMobile|Symbian|webOS/i));
	},

	/**
	 * Is the android app wrapper
	 */
	isAndroid: function() {
		return (navigator.userAgent.match(/LaBonoteca:android/i) || navigator.userAgent.match(/app-android/i));
	},

	/**
	 * Obtain from user agent string the android os version for device
	 */
	getAndroidOSVersion: function() {
		// Init var with 0.0 version to indicate that is not possible extract the OS version.
		var androidOSVersion = "0.0";
		var pattern = /android\s\d+(?:\.\d+){1,2}/i;

		if (navigator.userAgent.match(pattern))
			androidOSVersion = navigator.userAgent.match(pattern)[0].split(/\s/)[1];

		return androidOSVersion;
	},

	/**
	 * Populate the controller with needed libs
	 */
	controller: function(name, instance) {
		instance.name = name;
		instance.http = new Http(instance);
		instance.controllers = this.controllers;
	},

	/**
	 * Clean all in given instance
	 */
	dealloc: function(instance) {
		for(var prop in instance){
			instance[prop] = null;
			delete instance[prop];
		}
		instance = null;
	},

	/**
	 * add controllers to the app
	 * @param  {array} controllers array with the strings names controllers to add
	 * @author Alejo JM <alejo.jm@gmail.com>
	 */
	addControllers:function (controllers) {
		for (var a = 0; a < controllers.length; a++) {
			this.addController(controllers[a]);
		}
	},

	/**
	 * Add one controller to the Application
	 * @param  {string} controller name for add
	 * @author Alejo JM <alejo.jm@gmail.com>
	 */
	addController:function (controller) {
		if(!window[controller])
			throw new Error("Missing controller "+controller);

		this.controllers[controller] = new window[controller]();
	},

	/**
     * get declared instances
     */
	getController:function(name){
		if(this.controllers[name]){
			return this.controllers[name];
		}
		return false;
	},

	/**
	 * Trigger events
	 */
	trigger: function(eventType, info) {
		if (this.events[eventType])
			$(window).trigger(this.events[eventType], info);
	},

	/**
	 * Attach events
	 */
	event: function(eventType, handler) {
		if (this.events[eventType])
			$(window).bind(this.events[eventType], handler);
	},

    /**
     * register events to the app
     * @param  {string} eventType
     * @return {void}
     * @author Alejo    JM        <alejo.jm@gmail.com> August 02, 2013
     */
    register:function(eventName, handler){
		this.events[eventName] = eventName;
		this.event(eventName, handler);
    },

    /**
     * remove events was registered before
     * @param  {string} eventName
     * @param  {function} handler
     * @param  {boolean} removeFromEvents
     * @return {void}
     * @author Alejo    JM               <alejo.jm@gmail.com> August 04, 2013
     */
    remove:function(eventName, handler, removeFromEvents){
		$(window).unbind(eventName, handler);
		if(removeFromEvents && this.events[eventName])
			delete this.events[eventName];
    },


    /**
	 * events fired on every ajax come form the server, like "controllerName+AjaxResponse
	 * @param  {string} controllerName
	 * @param  {function} handler
	 * @return {void}
	 * @author Alejo    JM        <alejo.jm@gmail.com> August 02, 2013
	 */
	onResponse: function(controllerName, handler) {
		$(window).bind(controllerName+'AjaxResponse', handler);
	},


	/**
	 * Redirect to Facebook login URL
	 * @hashController: the hash controller that will be use by Facebook when redirect to the app
	 */
	loadFbLoginURL: function(hashController) {
		if (this.isAndroid())
			hashController += '&app-version=' + Config.version;

		var redirectUri = encodeURIComponent(Config.redirect_uri.replace('[FROM]', hashController));
		var logginURL = Config.url_facebook_login.replace('[REDIRECT_URI]', redirectUri);

		window.location = logginURL;
	},

	/**
	 * Redirect to some controller via hash change event
	 * @param  {string} controller name
	 * @param  {boolean} true/false default(false) addController redirect the hash and add controller if doesn't exists
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com>
	 */
	redirect: function(controller, addController) {
		var newHash = "";

		if (this.controllers[controller] && !addController)
			newHash = controller + ((this.isAndroid() && this.androidAppVersion !== "") ? "/app-version:" + this.androidAppVersion : "");
		else
			newHash = controller + ((this.isAndroid() && this.androidAppVersion !== "") ? "/app-version:" + this.androidAppVersion : "");

		//we have instance to handle the redirect
		var controllerName = Url.getHashController(controller);
		if(!this.getController(controllerName) && !addController){
			this.addController(controllerName);
		}

		if (newHash.length > 0)
			window.location.hash = newHash;
	},

	/**
	 * Methods called by AndroidApp wrapper, dipatch events to tell
	 * everyone interested on this ones
	 *
	 * @return {void}
	 * @author Alejo JM <alejo.jm@gmail.com> August 01, 2013
	 */
	onResume:function(){
		Log.console('Application.onResume');
		$(window).trigger('Application.onResume');
	},

	onPause:function(){
		Log.console('Application.onPause');
		$(window).trigger('Application.onPause');
	},

	onLowMemory:function(){
		Log.console('Application.onLowMemory');
		$(window).trigger('Application.onLowMemory');
	},

	/**
	 * Global methods for handle loading, blocking and ready,
	 */
	loading: function() {
		this.request = true;

		if (this.isAndroid() && this.androidAppVersion >= "1.7")
			AndroidApp.lockBackBtn();

		this.blockAll();
		this.showLoader();
	},

	loadingComplete: function() {
		this.request = false;

		if (this.isAndroid() && this.androidAppVersion >= "1.7") {
			new Timer({complete: function() {
				AndroidApp.unlockBackBtn();
			}, context: this, duration: 500});
		}

		this.ready();
		this.hideLoader();
	},

	blockAll: function() {
		$('.blockAll').removeClass('hide').css({height: $(document).height()});
	},

	ready: function() {
		$('.blockAll').css({height: ''}).addClass('hide');
	},

	showLoader: function() {
		$('.cargando').css({left: $(window).width() / 2, top: $(window).height() / 2}).removeClass('hide');
	},

	hideLoader: function() {
		$('.cargando').addClass('hide');
	}
};

/**
 * global window load, only here
 */
$(window).load($.proxy(Application.init,Application));
