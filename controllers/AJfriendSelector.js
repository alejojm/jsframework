/**
 * Load the ui is not included,
 * you can paste the content of AJfriendSelector.html in your html
 */
if (!$('#fs-dialog-box-wrap').length) {
    if (!$('#fs-wrapper-load').length)
        $('body').append('<div id="fs-wrapper-load"></div>');

    $('#fs-wrapper-load').load('AJfriendSelector.html', function(responseText, textStatus, xmlHttpRequest) {
        if (textStatus == 'error')
            throw new Error('missing file AJfriendSelector.html, put this file on your webroot');

        new AJfriendSelector();
    });
} else
    new AJfriendSelector();

/**
 * Object for manage facebook friends users
 * @author Alejo JM <alejo.jm@gmail.com> October 20, 2012
 */
function AJfriendSelector() {
    this.set();
    this.init();
}

/**
 * statics methods
 * @author Alejo JM <alejo.jm@gmail.com> October 20, 2013
 */
AJfriendSelector.setOptions = function(options) {
    if (!AJfriendSelector.options)
        AJfriendSelector.options = options;
    else
        this.instance.mergeOptions(options);
};

AJfriendSelector.setFriends = function(friends) {
    AJfriendSelector.isReady = true;
    AJfriendSelector.friends = friends;
};

AJfriendSelector.showInterface =  function() {
    this.instance.showFriends();
};

AJfriendSelector.close =  function() {
    this.instance.close();
};

AJfriendSelector.hide =  function() {
    this.instance.hide();
};

AJfriendSelector.hideFriend =  function(friendId) {
    this.instance.hideFriend(friendId);
};

AJfriendSelector.show =  function() {
    this.instance.show();
};

AJfriendSelector.facebookFeedComplete=function(friendId){
	this.instance.facebookFeedComplete(friendId);
};

/**
 * Object properties
 */
AJfriendSelector.prototype.set = function(options) {

    //runtime vars
    this.friends = [];
    this.overlay = $('#fs-overlay');
    this.wrap = $('#fs-dialog-box-wrap');
    this.content = $('#fs-dialog-box-content');
    this.selected_friend_count = 1;
    this.isShowSelectedActive = false;
    this.search_text_base = '';
    this.selected_friends = [];

    //object options
    this.options = {
        max: null,
        interfaceType: 'selectable',
        excludeIds: [],
        getStoredFriends: [],
        closeOverlayClick: true,
        enableEscapeButton: true,
        overlayOpacity: "0.3",
        overlayColor: '#000',
        closeOnSubmit: false,
        showSelectedCount: true,
        showButtonSelectAll: true,
        color: "default",
        onClose: null,
        onSubmit: null,
        context: this,
        maxFriendsCount: null,
        minSelectedFriends: null,
        showRandom: false,
        facebookInvite: false,
        lang: {
            title: "Friend Selector",
            buttonSubmit: "Send",
            buttonCancel: "Cancel",
            buttonSelectAll: "Select All",
            buttonDeselectAll: "Deselect All",
            buttonShowSelected: "Show Selected",
            buttonShowAll: "Show All",
            summaryBoxResult: "{1} best results for {0}",
            summaryBoxNoResult: "No results for {0}",
            searchText: "Enter a friend's name",
            fbConnectError: "You must connect to Facebook to see this.",
            selectedCountResult: "You have choosen {0} people.",
            selectedLimitResult: "Limit is {0} people.",
            facebookInviteMessage: "Invite message",
            missingSelectedFriends: "You must select at least {0} friends"
        }
    };
};

AJfriendSelector.prototype.init = function() {
	AJfriendSelector.instance = this;
    this.addLoading();
    this.mergeOptions();
    this.renderInterface();
    this.getFriends();
};

AJfriendSelector.prototype.addLoading = function() {
    //addloading
    $('#fs-user-list').append('<div id="fs-loading"></div>');
};

AJfriendSelector.prototype.removeLoading = function() {
    $('#fs-loading').remove();
};

AJfriendSelector.prototype.mergeOptions = function() {
    //merge local options with setting options
    var lang = this.options.lang;
    if (AJfriendSelector.options && AJfriendSelector.options.lang) {
        lang = $.extend(this.options.lang, this.options.lang, AJfriendSelector.options.lang);
    }

    var options = this.options;
    if (AJfriendSelector.options) {
        options = $.extend(this.options, this.options, AJfriendSelector.options);
    }

    this.options = options;
    this.options.lang = lang;
};

AJfriendSelector.prototype.renderInterface = function() {
    if (this.options.interfaceType != 'selectable') {
        $('#fs-submit-button').addClass('hide');
        $('#fs-cancel-button').addClass('hide');
        $('#fs-show-selected').addClass('hide');
    }
    $('#fs-dialog-title span').html(this.options.lang.title);
    $('#fs-input-text').attr('title', this.options.lang.searchText);
    $('#fs-show-selected span').html(this.options.lang.buttonShowSelected);
    $('#fs-select-all span').html(this.options.lang.buttonSelectAll);
    $('#fs-submit-button span').html(this.options.lang.buttonSubmit);
    $('#fs-cancel-button span').html(this.options.lang.buttonCancel);
};


AJfriendSelector.prototype.addEventListeners = function() {
    $(window).resize($.proxy(this.preResize, this));

    $('#fs-cancel-button').click($.proxy(this.close, this));
    $('#fs-submit-button').click($.proxy(this.submit, this));

    $('#fs-dialog input').focus(function() {
        if ($(this).val() === $(this)[0].title) {
            $(this).val('');
        }
    }).blur(function() {
        if ($(this).val() === '') {
            $(this).val($(this)[0].title);
        }
    }).blur();

    $('#fs-dialog input').keyup($.proxy(this.find, this));
    $('#fs-reset').click($.proxy(this.clearfind, this));
    $('#fs-user-list li').click($.proxy(this.click, this));
    $('#fs-show-selected').click($.proxy(this.showSelected, this));
    $(document).keyup($.proxy(this.testKey, this));

    if (this.options.closeOverlayClick === true) {
        this.overlay.css({
            'cursor': 'pointer'
        });
        this.overlay.click($.proxy(this.close, this));
    }
};

AJfriendSelector.prototype.stopEvent = function() {
    $('#fs-reset').unbind("click");
    $('#fs-user-list li').unbind("click");
    this.selected_friend_count = 1;
    $('#fs-select-all').unbind("click");
};

AJfriendSelector.prototype.clearfind = function() {
    $('#fs-input-text').val('');
    $('#fs-summary-box').remove();
    if(this.options.interfaceType == 'selectable'){
		$('#fs-user-list ul li').removeClass('hide');
    }
    else{
		$('#fs-user-list ul li').each(function(){
			if(!$(this).hasClass('hasFeed'))
				$(this).removeClass('hide');
		});
    }
    $('#fs-input-text').blur();
};

AJfriendSelector.prototype.testKey = function(e) {
    if (e.which === 27 && this.options.enableEscapeButton === true) {
        this.close();
    }
};

AJfriendSelector.prototype.find = function() {
    var t = $('#fs-dialog input');
    var fs_dialog = $('#fs-dialog');

    this.search_text_base = $.trim(t.val());
    var search_text = this.search_text_base.toLowerCase().replace(/\s/gi, '+');

    var elements = $('#fs-user-list .fs-fullname[value*=' + search_text + ']');

    var container = $('#fs-user-list ul');
    container.children().addClass('hide');
    $.each(elements, function() {
        $(this).parents('li').removeClass('hide');
    });

    var result_text = '';
    if (elements.length > 0 && this.search_text_base > '') {
        result_text = this.options.lang.summaryBoxResult.replace("{0}", '"' + t.val() + '"');
        result_text = result_text.replace("{1}", elements.length);
    } else {
        result_text = this.options.lang.summaryBoxNoResult.replace("{0}", '"' + t.val() + '"');
    }


    if (!fs_dialog.has('#fs-summary-box').length) {
        $('#fs-filter-box').after('<div id="fs-summary-box"><span id="fs-result-text">' + result_text + '</span></div>');
    } else if (!fs_dialog.has('#fs-result-text').length) {
        $('#fs-summary-box').prepend('<span id="fs-result-text">' + result_text + '</span>');
    } else {
        $('#fs-result-text').text(result_text);
    }

    if (this.search_text_base === '') {
        if (fs_dialog.has('#fs-summary-box').length) {

            if (this.selected_friend_count === 1) {
                $('#fs-summary-box').remove();
            } else {
                $('#fs-result-text').remove();
            }

        }
    }
};

AJfriendSelector.prototype.getFriends = function() {

    if (!AJfriendSelector.isReady) return;

    var friends = AJfriendSelector.friends;
    if (!friends) {
        throw new Error('missing friends !!! tip: AJfriendSelector.setFriends([{"name":"friend.name","id":"facebok_id"}])');
    }

    if (typeof friends == 'string') {
        throw new Error('friends is not a array !!! tip: AJfriendSelector.setFriends([{"name":"friend.name","id":"facebok_id"}])');
    }

    var arrayFriends = [];
    for (var a in friends) {
        arrayFriends[a] = friends[a];
    }

    if (!arrayFriends.length) {
        throw new Error('friends is not a array !!! tip: AJfriendSelector.setFriends([{"name":"friend.name","id":"facebok_id"}])');
    }

    this.friends = arrayFriends;
    this.showFriends();
    this.activateSelectAll();
};

AJfriendSelector.prototype.showFriends = function() {

    $('#fs-dialog-buttons').removeClass('hide');

    //no friends try again
    if (!this.friends.length) {
        this.getFriends();
        return;
    }

	//the ui was render before and is populated ? only show the ui
	if($('#fs-user-list ul li').length == this.friends.length)
		return this.show();

    this.resize(true);
    var facebook_friends = this.friends;
    var max_friend_control = this.options.maxFriendsCount !== null && this.options.maxFriendsCount > 0;
    if (this.options.showRandom === true || max_friend_control === true) {
        facebook_friends = this.shuffleData();
    }

    for (var i = 0, k = 0; i < facebook_friends.length; i++) {
        if (max_friend_control && this.options.maxFriendsCount <= k) {
            break;
        }
        if ($.inArray(parseInt(facebook_friends[i].id, 10), this.options.getStoredFriends) >= 0) {
            this.setFacebookFriends(i, facebook_friends, true);
            k++;
        }
    }

    for (var j = 0; j < facebook_friends.length; j++) {

        if (max_friend_control && this.options.maxFriendsCount <= j + this.options.getStoredFriends.length) {
            break;
        }
        if ($.inArray(parseInt(facebook_friends[j].id, 10), this.options.excludeIds) >= 0) {
            continue;
        }

        if ($.inArray(parseInt(facebook_friends[j].id, 10), this.options.getStoredFriends) <= -1) {
            this.setFacebookFriends(j, facebook_friends, false);
        }
    }

    this.removeLoading();
    this.addEventListeners();
};


AJfriendSelector.prototype.showSelected = function(e) {

    var t = $(e.target);
    var container = $('#fs-user-list ul'),
        allElements = container.find('li'),
        selectedElements = container.find('li.checked');

    if (selectedElements.length !== 0 && selectedElements.length !== allElements.length || this.isShowSelectedActive === true) {
        if (this.isShowSelectedActive === true) {
            t.removeClass('active').text(this.options.lang.buttonShowSelected);

            container.children().show();

            this.isShowSelectedActive = false;
        } else {
            t.addClass('active').text(this.options.lang.buttonShowAll);

            container.children().hide();
            $.each(selectedElements, function() {
                $(this).show();
            });

            this.isShowSelectedActive = true;
        }
    }
};

AJfriendSelector.prototype.activateSelectAll = function() {
    if (this.options.showButtonSelectAll === true && this.options.max === null) {
        $('#fs-show-selected').before('<a href="javascript:{}" id="fs-select-all"><span>' + this.options.lang.buttonSelectAll + '</span></a> - ');
        $('#fs-select-all').click($.proxy(this.selectAll, this));
    }
};

AJfriendSelector.prototype.selectAll = function(e) {

    if (this.selected_friend_count - 1 !== $('#fs-user-list li').length) {

        $('#fs-user-list li:hidden').show();
        this.resetSelection();

        var lis = $('#fs-user-list li');
        for (var a = 0; a < lis.length; a++) {
            this.click(lis[a]);
        }
        $('#fs-select-all').text(this.options.lang.buttonDeselectAll);

        if (this.isShowSelectedActive === true) {
            isShowSelectedActive = false;
            $('#fs-show-selected').text(this.options.lang.buttonShowSelected);
        }
    } else {
        this.resetSelection();
        this.showFriendCount();
        $('#fs-select-all').text(this.options.lang.buttonSelectAll);
    }
};

AJfriendSelector.prototype.resetSelection = function() {
    $('#fs-user-list li').removeClass('checked');
    $('#fs-user-list input.fs-friends').attr('checked', false);
    this.selected_friend_count = 1;
};

AJfriendSelector.prototype.shuffleData = function() {
    var array_data = this.friends;
    for (var j, x, i = array_data.length; i; j = parseInt(Math.random() * i, 10), x = array_data[--i], array_data[i] = array_data[j], array_data[j] = x);
    return array_data;
};

AJfriendSelector.prototype.setFacebookFriends = function(k, v, predefined) {
    var item = $('<li/>');
    var showCheckbox = this.options.interfaceType == 'selectable' ? '' : 'hide';
    var link = '<a class="fs-anchor" href="javascript://">' + '<input class="fs-fullname" type="hidden" name="fullname[]" value="' + v[k].name.toLowerCase().replace(/\s/gi, "+") + '" />' + '<input class="fs-friends ' + showCheckbox + '" type="checkbox" name="friend[]" value="fs-' + v[k].id + '" />' + '<img class="fs-thumb" src="https://graph.facebook.com/' + v[k].id + '/picture" />' + '<span class="fs-name">' + this.charLimit(v[k].name, 15) + '</span>' + '</a>';
    item.append(link);

    $('#fs-user-list ul').append(item);

    if (predefined) {
        this.click(item);
    }
};

AJfriendSelector.prototype.charLimit = function(word, limit) {

    var wlen = word.length;

    if (wlen <= limit) {
        return word;
    }

    return word.substr(0, limit) + '...';

};

AJfriendSelector.prototype.limitText = function() {
    if (this.selected_friend_count > this.options.max && this.options.max !== null) {
        var selected_limit_text = this.options.lang.selectedLimitResult.replace('{0}', this.options.max);
        $('.fs-limit').html('<span class="fs-limit fs-full">' + selected_limit_text + '</span>');
        return false;
    }
};

AJfriendSelector.prototype.showFriendCount = function() {
    if (this.selected_friend_count > 1 && this.options.showSelectedCount === true) {

        var selected_count_text = this.options.lang.selectedCountResult.replace('{0}', (this.selected_friend_count - 1));

        if (!$('#fs-dialog').has('#fs-summary-box').length) {
            $('#fs-filter-box').after('<div id="fs-summary-box"><span class="fs-limit fs-count">' + selected_count_text + '</span></div>');
        } else if (!$('#fs-dialog').has('.fs-limit.fs-count').length) {
            $('#fs-summary-box').append('<span class="fs-limit fs-count">' + selected_count_text + '</span>');
        } else {
            $('.fs-limit').text(selected_count_text);
        }
    } else {

        if (this.search_text_base === '') {
            $('#fs-summary-box').remove();
        } else {
            $('.fs-limit').remove();
        }

    }
};

AJfriendSelector.prototype.showMissingSelectedFriends = function() {


    var selected_count_text = this.options.lang.missingSelectedFriends.replace('{0}', this.options.minSelectedFriends);

    if (!$('#fs-dialog').has('#fs-summary-box').length) {
        $('#fs-filter-box').after('<div id="fs-summary-box"><span class="fs-limit fs-count">' + selected_count_text + '</span></div>');
    } else if (!$('#fs-dialog').has('.fs-limit.fs-count').length) {
        $('#fs-summary-box').append('<span class="fs-limit fs-count">' + selected_count_text + '</span>');
    } else {
        $('.fs-limit').text(selected_count_text);
    }
};

AJfriendSelector.prototype.click = function(e) {
    var btn = $(e);
    if (e.target) {
        btn = $(e.target).parents().filter('li');
    }

    if (btn.hasClass('checked')) {

        btn.removeClass('checked');
        btn.find('input.fs-friends').attr('checked', false);

        if (this.options.interfaceType == 'selectable') {
            this.selected_friend_count--;
        }

        if (this.selected_friend_count - 1 !== $('#fs-user-list li').length) {
            $('#fs-select-all').text(this.options.lang.buttonSelectAll);
        }

    } else {

        var limit_state = this.limitText();

        if (limit_state === false) {
            return false;
        }

        if (this.options.interfaceType == 'selectable') {
            this.selected_friend_count++;
            btn.addClass('checked');
        }

        var checkbox = btn.find('input.fs-friends');
        checkbox.attr('checked', true);
        $(window).trigger('selectedFriend', checkbox.val().replace('fs-', ''));
    }

    if (this.options.interfaceType == 'selectable')
        this.showFriendCount();
};


AJfriendSelector.prototype.hideFriend = function(friendId){
    var limit_state = this.limitText();
    if (limit_state === false) {
        return false;
    }

    this.selected_friend_count++;

    var input = $('[value="fs-' + friendId + '"]');
	input.attr('checked', true);
    input.parent().parent().addClass('hide hasFeed');
    $('#fs-reset').trigger('click');
    this.showFriendCount();
};

AJfriendSelector.prototype.getSelectedFriends = function(e) {
    var selected_friends = [];
    $('input.fs-friends:checked').each(function() {
        var splitId = $(this).val().split('-');
        var id = splitId[1];
        selected_friends.push(parseInt(id, 10));
    });

    this.selected_friends = selected_friends;
    return this.selected_friends;
};

AJfriendSelector.prototype.submit = function(e) {
    //get selected friends
    this.getSelectedFriends();

    //update options maybe change
    this.mergeOptions();

    //we need count minimum selected friends ?
    if (this.options.minSelectedFriends && (this.selected_friend_count - 1) < this.options.minSelectedFriends) {
        return this.showMissingSelectedFriends();
    }

    //execute callback if exists
    if (this.options.onSubmit) this.options.onSubmit.call(this.options.context, this.selected_friends);

    if (this.options.closeOnSubmit)
        this.close();
    else
        this.uiForAddLoading();
};

AJfriendSelector.prototype.uiForAddLoading = function() {
    $('#fs-dialog-buttons').addClass('hide');
    this.addLoading();
};

AJfriendSelector.prototype.close = function(e) {
    this.removeLoading();
    //clean the container friends
    $('#fs-user-list ul').html('');
    this.wrap.fadeOut(400, $.proxy(this.closeComplete, this));
    this.isShowSelectedActive = false;
    this.onClose();
};

AJfriendSelector.prototype.closeComplete = function() {
    this.wrap.addClass('hide');
    this.stopEvent();
    this.overlay.fadeOut(400, function() {
        $(this).addClass('hide');
    });
};

AJfriendSelector.prototype.hide = function(e) {
	this.wrap.fadeOut(400);
    this.overlay.delay(200).fadeOut(400, function() {
        $(this).addClass('hide');
    });

};

AJfriendSelector.prototype.show = function(e) {
	this.wrap.fadeIn();
    this.overlay.fadeIn();
};

AJfriendSelector.prototype.onClose = function() {
    //update options maybe change
    this.mergeOptions();

    if (this.options.onClose)
		this.options.onClose.call(this.options.context);
};

AJfriendSelector.prototype.preResize = function(e) {
    this.resize(false);
};

AJfriendSelector.prototype.resize = function(is_started) {
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var docHeight = $(document).height(),
        wrapWidth = this.wrap.width(),
        wrapHeight = this.wrap.height(),
        wrapLeft = (windowWidth / 2) - (wrapWidth / 2),
        wrapTop = (windowHeight / 2) - (wrapHeight / 2);

    if (is_started === true) {
        this.overlay.css({
            'background-color': this.options.overlayColor,
            'opacity': this.options.overlayOpacity,
            'height': docHeight
        }).fadeIn('fast', function() {
            $('#fs-dialog-box-wrap').css({
                left: wrapLeft,
                top: wrapTop
            }).fadeIn();
        });
    } else {
        this.wrap.stop().animate({
            left: wrapLeft,
            top: wrapTop
        }, 200);
        this.overlay.css({
            'height': docHeight
        });
    }
};